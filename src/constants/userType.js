export const USER_TYPE = {
  SUPER_ADMIN: 'SUPER_ADMIN',
  ADMIN: 'ADMIN',
  NORMAL_USER: 'NORMAL_USER',
  NORMAL_USER_NOT_VALID: 'NORMAL_USER_NOT_VALID',
  VISITOR_BANNED: 'VISITOR_BANNED',
};

export const USER_TYPE_INT = {
  SUPER_ADMIN: 0,
  ADMIN: 1,
  NORMAL_USER: 2,
  NORMAL_USER_NOT_VALID: 3,
  VISITOR_BANNED: 4,
};

export const toUserType = (number) => {
  return Object.keys(USER_TYPE)[number];
};
