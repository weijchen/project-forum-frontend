import React from 'react';
import './App.css';
import { Route, RouterProvider, createRoutesFromElements, Routes, Router } from 'react-router';

import { createBrowserRouter } from 'react-router-dom';
import Home from './containers/Home/Home';
import User from './containers/User/User';
import Login from './containers/Auth/Login.jsx';
import Register from './containers/Auth/Register.jsx';
import AppLayout from './components/AppLayout.jsx';
import { Provider } from 'react-redux';
import store from './redux/store/store';
import UserManagement from './containers/User/UserManagement';
import MessageManagement from './containers/Message/MessageManagement';

import PostDetail from './components/PostDetail/PostDetail';
import ProtectedRoute from './components/ProtectedRoute.jsx';
import ContactUs from './containers/Message/ContactUs';
import { USER_TYPE } from './constants/userType';

// Router via JSX
const router = createBrowserRouter(
  createRoutesFromElements(
    <Route element={<AppLayout />}>
      <Route exact path="/contactus" element={<ContactUs />} />
      <Route exact path="/login" element={<Login />} />
      <Route exact path="/register" element={<Register />} />

      <Route
        element={
          <ProtectedRoute
            redirectPath="/"
            authorizedRoles={[
              USER_TYPE.SUPER_ADMIN,
              USER_TYPE.ADMIN,
              USER_TYPE.NORMAL_USER,
              USER_TYPE.NORMAL_USER_NOT_VALID,
            ]}
          />
        }
      >
        <Route exact path="/" element={<Home />} />
        <Route exact path="*" element={<Home />} />
        <Route exact path="/users/:userId/profile" element={<User />} />
        <Route exact path="/posts/:postId" element={<PostDetail />} />
      </Route>

      <Route element={<ProtectedRoute redirectPath="/" authorizedRoles={[USER_TYPE.SUPER_ADMIN, USER_TYPE.ADMIN]} />}>
        <Route exact path="/users" element={<UserManagement />} />
        <Route exact path="/messages" element={<MessageManagement />} />
      </Route>
    </Route>,
  ),
);

function App(props) {
  return (
    <div className="App">
      <Provider store={store}>
        <RouterProvider router={router} />
      </Provider>
    </div>
  );
}

export default App;
