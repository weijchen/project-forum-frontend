import axios from 'axios';
import store from '../redux/store/store';
import { SET_TOKEN_VALID } from '../redux/actions/actionTypes';

let HTTPstatus = null;

axios.defaults.baseURL = 'http://localhost:9000/';
axios.defaults.headers.common['Content-Type'] = 'application/json';
axios.defaults.headers.common.Accept = 'application/json';
axios.defaults.headers.common.Authorization = 'Bearer ' + window.localStorage.getItem('token');
axios.interceptors.response.use(
  (res) => {
    HTTPstatus = res.status;
    return res;
  },
  (error) => {
    console.error('error:', error);
    if (error && error.response && error.response.data) {
      HTTPstatus = error.response.status;
      if (HTTPstatus === 401) {
        window.localStorage.clear();
        store.dispatch({ type: SET_TOKEN_VALID, isTokenValid: false });
      }
      throw error.response.data;
    }

    throw error;
  },
);

const Auth = {
  login: async (body) => {
    const result = await axios.post('auth-service/auth/login', body);
    if (result.data.success) {
      axios.defaults.headers.common.Authorization = 'Bearer ' + result.data.data;
    }
    return result;
  },
  register: async (body) => {
    return await axios.post('auth-service/auth/register', body);
  },
};

const User = {
  getAllUsers: async () => axios.get('user-service/users'),
  getUserProfile: async (params) => axios.get('user-service/user', { params }),
  updateUserProfile: async (id, firstName, lastName, email, password, formData) => {
    const customHeaders = {
      'Content-Type': 'multipart/form-data',
    };
    return await axios.patch(
      `composite-service/users/${id}?firstName=${firstName}&lastName=${lastName}&email=${email}&password=${password}`,
      formData,
      {
        headers: customHeaders,
      },
    );
  },
  promoteUser: async (id) => {
    return await axios.patch(`user-service/users/${id}/promote`);
  },
  banUnbanUser: async (id) => {
    return await axios.patch(`user-service/users/${id}/active`);
  },
  getUserGeneralInfoById: async (id) => axios.get(`user-service/user/${id}/general`),
  sendVerificationEmail: async (email, body) => axios.post(`user-service/validate/send/?email=${email}`, body),
};

const Post = {
  getAllPublishedPosts: async () => axios.get('post-service/'),
  getAllPublishedPostsWithPages: async (page, size) => axios.get(`post-service/with-jpa/?page=${page}&size=${size}`),
  getAllPublishedPostsWithPagesSortByDate: async (page, size, filterStatus) =>
    axios.get(`post-service/with-jpa/sortByDates/?page=${page}&size=${size}&filterStatus=${filterStatus}`),
  getAllPublishedPostWithPagesSortByReplies: async (page, size, filterStatus) =>
    axios.get(`post-service/with-jpa/sortByReplies/?page=${page}&size=${size}&filterStatus=${filterStatus}`),
  getAllTypedPostsWithPagesSortByDate: async (postStatus, page, size, filterStatus) =>
      axios.get(`post-service/with-jpa/sortByDates/?postStatus=${postStatus}&page=${page}&size=${size}&filterStatus=${filterStatus}`),
  getAllTypedPostWithPagesSortByReplies: async (postStatus, page, size, filterStatus) =>
      axios.get(`post-service/with-jpa/sortByReplies/?postStatus=${postStatus}&page=${page}&size=${size}&filterStatus=${filterStatus}`),

  getUnpublishedPosts: async () => axios.get('post-service/draft'),
  getThreePostWithMostReviews: async () => axios.get('post-service/top3'),
  getPublishedPosts: async () => axios.get('post-service/'),
  getUserPublishedPosts: async () => axios.get('post-service/myPosts'),
  getUserBannedPosts: async () => axios.get('post-service/myPosts/banned'),
  getUserHiddenPosts: async () => axios.get('post-service/myPosts/hidden'),
  getUserDeletedPosts: async () => axios.get('post-service/myPosts/deleted'),
  getPostByPostId: async (id) => axios.get(`composite-service/posts/${id}`),
  createReply: async (id, body) => axios.post(`post-service/posts/${id}/replies`, body),
  updateHistory: async (body) => {
    console.log(body);
    return await axios.post(`history-service/addOrUpdate`, body);
  },
  createsubReply: async (id, idx, body) => axios.post(`post-service/posts/${id}/replies/${idx}/sub-replies`, body),
  getFileById: async (id) => axios.get(`file-service/download/${id}`),
  updatePost: async (id, body) => axios.patch(`post-service/${id}`, body),
  deleteReply: async (pid, rpid) => axios.delete(`post-service/posts/${pid}/replies/${rpid}`),
  deleteSubReply: async (pid, rpid, srpid) =>
    axios.delete(`post-service/posts/${pid}/replies/${rpid}/sub-replies/${srpid}`),
  updateCache: async (id) => axios.get(`/composite-service/cache/${id}`),
  archivePost: async (id) => axios.patch(`post-service/${id}/archive`),
  hidePost: async (id) => axios.patch(`post-service/${id}/hidden`),
  publishPost: async (id) => axios.patch(`post-service/${id}/publish`),
  banPost: async (id) => axios.patch(`post-service/${id}/banned`),
  getAllTypePosts: async (postStatus, page, size) =>
    axios.get(`/post-service/with-jpa?postStatus=${postStatus}&page=${page}&size=${size}`),
  deletePost: async (id) => axios.patch(`post-service/${id}/delete`),
  recoverPost: async (id) => axios.patch(`post-service/${id}/recover`),
};

const Message = {
  getAllMessages: async () => axios.get('message-service/messages/all'),
  updateMessageStatus: async (messageId) => axios.patch(`message-service/messages/${messageId}/status`),
  createMessage: async (body) => axios.post('message-service/messages', body),
};

const Composite = {
  getViewedPost: async () => axios.get('composite-service/history'),

  // newPost: async (body) => {
  //   const customHeaders = {
  //     'Content-Type': 'multipart/form-data',
  //   };
  //   return await axios.post('composite-service/posts', body, {
  //     headers: customHeaders,
  //   });
  // },
  newPost: async(body) => axios.post('composite-service/posts', body, {
    headers: {'Content-Type': 'multipart/form-data',},
  }),
  getAllPublishedPostsWithPagesFilterByName: async (page, size, username) =>
    axios.get(`composite-service/with-jpa/filterByName/?page=${page}&size=${size}&username=${username}`),

  getAllTypedPostsWithPagesFilterByName: async (postStatus, page, size, username) =>
      axios.get(`composite-service/with-jpa/filterByName/?postStatus=${postStatus}&page=${page}&size=${size}&username=${username}`),

  getViewedPostOnDate: async (date) => axios.get(`composite-service/historyByDate?date=${date}`),
  getViewedPostContainWord: async (word) => axios.get(`composite-service/historyContainWord?word=${word}`),
};

const File = {
  postImage: async (body) => {
    const customHeaders = {
      'Content-Type': 'multipart/form-data',
    };
    return await axios.post('file-service/uploadUrl', body, {
      headers: customHeaders,
    });
  },
};

export default {
  Auth,
  User,
  Post,
  Message,
  Composite,
  File,
};
