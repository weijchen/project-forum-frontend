import { LOGIN, LOGOUT, SET_TOKEN_VALID, SET_CURRENT_USER } from '../actions/actionTypes.js';

const initialState = {
  isTokenValid: false,
  userInfo: {
    email: null,
    role: null,
    userId: null,
    firstName: null,
    lastName: null,
  },
};

export default (state = initialState, action) => {
  switch (action.type) {
    case LOGIN:
      try {
        return {
          ...state,
          isTokenValid: true,

          userInfo: {
            email: action.payload.email,
            role: action.payload.role,
            userId: action.payload.userId,
            firstName: action.payload.firstName,
            lastName: action.payload.lastName,
          },
        };
      } catch (error) {
        return { ...state, isTokenValid: false, userinfo: {} };
      }

    case LOGOUT: {
      return {
        ...state,
        isTokenValid: false,
        userinfo: {},
      };
    }

    case SET_TOKEN_VALID:
      return {
        ...state,
        isTokenValid: action.isTokenValid,
      };

    case SET_CURRENT_USER:
      try {
        return {
          ...state,
          userInfo: {
            email: action.payload.email,
            role: action.payload.role,
            userId: action.payload.userId,
            firstName: action.payload.firstName,
            lastName: action.payload.lastName,
          },
        };
      } catch (error) {
        return { ...state, isTokenValid: false, userinfo: {} };
      }
    default:
      return state;
  }
};
