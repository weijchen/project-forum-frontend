const initialState = {
  posts: [],
};

const postReducer = (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case 'ADD_POST':
      return {
        ...state,
        posts: [...state.posts, payload],
      };
    case 'DELETE_POST':
      return {
        ...state,
        posts: state.posts.filter((post) => post.id !== payload),
      };
    default:
      return state;
  }
};

export default postReducer;
