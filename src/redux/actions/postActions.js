import { createAction } from "../store/store"
import { ADD_POST } from "./actionTypes"

export const addPostAction = (data) => {
  createAction(ADD_POST, data);
}
