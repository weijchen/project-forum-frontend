// Post
export const ADD_POST = 'ADD_POST';
export const DELETE_POST = 'DELETE_POST';

// Auth
export const LOGOUT = 'LOGOUT';
export const SET_TOKEN_VALID = 'SET_TOKEN_VALID';
export const SET_CURRENT_USER = 'SET_CURRENT_USER';
export const LOGIN = 'LOGIN';
