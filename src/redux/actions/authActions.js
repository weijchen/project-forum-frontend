// redux/authActions.js
import { createAction } from '../store/store';
import { LOGIN, LOGOUT, SET_TOKEN_VALID, SET_CURRENT_USER } from './actionTypes';

export const loginAction = (data) => {
  return createAction(LOGIN, data);
};

export const logoutAction = () => {
  return createAction(LOGOUT, null);
};
export const setTokenValid = (isTokenValid) => {
  createAction(SET_TOKEN_VALID, isTokenValid);
};
export const setCurrentUserAction = (userInfo) => {
  createAction(SET_CURRENT_USER, userInfo);
};
