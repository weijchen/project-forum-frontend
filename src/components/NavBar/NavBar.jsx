import React from 'react';
import { Link } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import { useDispatch } from 'react-redux';
import { logoutAction } from '../../redux/actions/authActions.js';
import { useSelector } from 'react-redux';
import { persistor } from '../../redux/store/store.js';
import { USER_TYPE } from '../../constants/userType.js';
import logo from '../../assets/icons/icon_2_cropped.png';

const Navbar = () => {
  const dispatch = useDispatch();

  const isTokenValid = useSelector((state) => state.auth.isTokenValid);
  const userRole = useSelector((state) => state.auth.userInfo.role);
  const userId = useSelector((state) => state.auth.userInfo.userId);
  const logOut = async (e) => {
    console.log('Logout, clear all credentials');
    window.localStorage.clear();
    dispatch(logoutAction());
    persistor.pause();
    persistor.flush().then(() => {
      return persistor.purge();
    });
    window.location.href = '/login';
  };

  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-light">
      <Link to={isTokenValid ? '/home' : '/login'} className="navbar-brand">
        {/*    Our icon or label   */}
        <img style={{ width: '120px' }} src={logo} alt="example" />
        {/* WePost */}
      </Link>
      {isTokenValid ? (
        <>
          <div className="navbar-nav mr-auto">
            <li className="nav-item">
              <Link to="/home" className="nav-link">
                Home
              </Link>
            </li>

            {userRole.toUpperCase() === USER_TYPE.SUPER_ADMIN || userRole.toUpperCase() === USER_TYPE.ADMIN ? (
              <div className="navbar-nav mr-auto">
                <li className="nav-item">
                  <Link to={'/users'} className="nav-link">
                    User Management
                  </Link>
                </li>
                <li className="nav-item">
                  <Link to={'/messages'} className="nav-link">
                    Message Management
                  </Link>
                </li>
              </div>
            ) : (
              <div className="navbar-nav mr-auto">
                <li className="nav-item">
                  <Link to={`/users/${userId}/profile`} className="nav-link">
                    Profile
                  </Link>
                </li>
                {/* <li className="nav-item">
                  <Link to={'/posts'} className="nav-link">
                    Post
                  </Link>
                </li> */}
                <li className="nav-item">
                  <Link to={'/contactus'} className="nav-link">
                    Contact Us
                  </Link>
                </li>
              </div>
            )}
          </div>

          <div className="navbar-nav ml-auto">
            <li className="nav-item">
              <Link className="nav-link" onClick={logOut}>
                LogOut
              </Link>
            </li>
          </div>
        </>
      ) : (
        <div className="navbar-nav ml-auto">
          <li className="nav-item">
            <Link to="/login" className="nav-link">
              Login
            </Link>
          </li>
          <li className="nav-item">
            <Link to="/register" className="nav-link">
              Register
            </Link>
          </li>
          <li className="nav-item">
            <Link to={'/contactus'} className="nav-link">
              Contact Us
            </Link>
          </li>
        </div>
      )}
    </nav>
  );
};

export default Navbar;
