import React, { useEffect } from "react";
import { Outlet, useLocation } from "react-router-dom";
import Navbar from "./NavBar/NavBar.jsx";

function AppLayout() {
    const location = useLocation();

    useEffect(() => {
        console.log("Applayout useeffect(): location =", location);
    }, [location]);

    return (
        <>
            <Navbar />
            <Outlet />
        </>
    )
}

export default AppLayout;
