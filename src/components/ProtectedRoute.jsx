import { Navigate, Outlet } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { USER_TYPE } from '../constants/userType';

const ProtectedRoute = ({ redirectPath, children, type, authorizedRoles, ...rest }) => {
  const isTokenValid = useSelector((state) => state.auth.isTokenValid);
  const userRole = useSelector((state) => state.auth.userInfo.role);
  const curPath = window.location.pathname;

  if (userRole == null) {
    return <Navigate to={'/login'} replace />; 
  }

  const isAuthorized = authorizedRoles.includes(userRole.toUpperCase());

  if (!isAuthorized) {
    return <Navigate to={redirectPath} replace />;
  }

  return children ? children : <Outlet />;
};
export default ProtectedRoute;
