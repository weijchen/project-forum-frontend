import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import PropTypes from 'prop-types';
import LoadingModal from '../Common/LoadingModal.jsx';
import './auth.css';
const Register = (props) => {
  const { isWaiting, errorMsg, register } = props;
  const navigateTo = useNavigate();
  const [formData, setFormData] = useState({
    firstName: '',
    lastName: '',
    email: '',
    password: '',
  });
  const [errors, setErrors] = useState('');
  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setFormData((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };
  const validateForm = () => {
    const errors = {};

    // Check if firstname is empty
    if (!formData.firstName.trim()) {
      errors.firstName = 'Firstname is required';
    }
    // Check if lastname is empty
    if (!formData.lastName.trim()) {
      errors.lastName = 'Lastname is required';
    }

    // Check if email is empty
    if (!formData.email.trim()) {
      errors.email = 'Email is required';
    } else if (!isValidEmail(formData.email)) {
      errors.email = 'Invalid email address.';
    }

    // Check if password is empty
    if (!formData.password.trim()) {
      errors.password = 'Password is required';
    }

    // Add more validation rules as needed (e.g., email format, password length, etc.)

    return errors;
  };

  const isValidEmail = (email) => {
    // You can add more sophisticated email validation here if needed.
    return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email);
  };

  const handleRegister = async (e) => {
    e.preventDefault();

    const formErrors = validateForm();
    setErrors(formErrors);

    const validForm = Object.keys(formErrors).length === 0;

    if (validForm) {
      try {
        const response = await register(formData.firstName, formData.lastName, formData.email, formData.password);
        if (response.success) {
          navigateTo('/login');
        } else {
          toast.error(response.message);
          console.error('Register failed: ' + response.message);
        }
      } catch (error) {
        setErrors(error.response.data.message);
      }
    }
  };
  return (
    <>
      <LoadingModal isWaiting={isWaiting} errorMsg={errorMsg} />

      <div className="col-md-12">
        <div className="card card-container">
          <h1>Register</h1>
          <form onSubmit={handleRegister}>
            <div className="form-group">
              <label htmlFor="firstName">Firstname:</label>
              <input
                type="text"
                className="form-control"
                id="firstName"
                name="firstName"
                value={formData.firstName}
                onChange={handleInputChange}
              />
              {errors.firstName && <div className="error">{errors.firstName}</div>}
            </div>

            <div className="form-group">
              <label htmlFor="lastName">Lastname:</label>
              <input
                type="text"
                className="form-control"
                id="lastName"
                name="lastName"
                value={formData.lastName}
                onChange={handleInputChange}
              />
              {errors.lastName && <div className="error">{errors.lastName}</div>}
            </div>

            <div className="form-group">
              <label htmlFor="email">Email:</label>
              <input
                type="email"
                className="form-control"
                id="email"
                name="email"
                value={formData.email}
                onChange={handleInputChange}
              />
              {errors.email && <div className="error">{errors.email}</div>}
            </div>

            <div className="form-group">
              <label htmlFor="password">Password:</label>
              <input
                type="password"
                className="form-control"
                id="password"
                name="password"
                value={formData.password}
                onChange={handleInputChange}
              />
              {errors.password && <div className="error">{errors.password}</div>}
            </div>
            <button type="submit" className="btn btn-primary btn-block">
              <span>Register</span>
            </button>
          </form>
        </div>
      </div>
    </>
  );
};

Register.protoTypes = {
  isWaiting: PropTypes.bool.isRequired,
  errorMsg: PropTypes.string.isRequired,
  register: PropTypes.func.isRequired,
};

export default Register;
