// components/login.jsx
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { resolveToken } from '../../utils/utils.js';

import { useDispatch } from 'react-redux';
import { loginAction } from '../../redux/actions/authActions.js';
import LoadingModal from '../Common/LoadingModal.jsx';
import './auth.css';
const Login = (props) => {
  const { isWaiting, errorMsg, login, getUserProfileById } = props;
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [errors, setErrors] = useState({});
  const navigateTo = useNavigate();
  const dispatch = useDispatch();

  const validateForm = () => {
    const errors = {};

    // Check if email is empty
    if (!email.trim()) {
      errors.email = 'Email is required';
    }

    // Check if password is empty
    if (!password.trim()) {
      errors.password = 'Password is required';
    }

    // Add more validation rules as needed (e.g., email format, password length, etc.)

    return errors;
  };

  const handleLogin = async (e) => {
    e.preventDefault();

    const formErrors = validateForm();
    setErrors(formErrors);
    if (Object.keys(formErrors).length > 0) {
      return;
    }
    try {
      // Call the authService login function here with email and password
      const response = await login(email, password);
      if (response.success) {
        localStorage.setItem('token', response.data);

        const user = resolveToken();

        try {
          const profileResponse = await getUserProfileById(user.userId);
          if (profileResponse.success) {

            const userInfo = {
              email: user.email,
              role: user.role,
              userId: user.userId,
              firstName: profileResponse.data.firstName,
              lastName: profileResponse.data.lastName,
            };
            dispatch(loginAction(userInfo));
            navigateTo('/home');
          } else {
            toast.error(profileResponse.message);
          }
        } catch (error) {
          toast.error(error.message);
        }
      } else {
        toast.error('Login failed: ' + response.message);
      }
    } catch (error) {
      // Handle login errors here
      toast.error('Login failed:', error.message);
    }
  };

  return (
    <>
      <LoadingModal isWaiting={isWaiting} errorMsg={errorMsg} />

      <div className="col-md-12">
        <div className="card card-container">
          <h1>Login</h1>
          <form onSubmit={handleLogin}>
            <div className="form-group">
              <label htmlFor="email">Email</label>
              <input
                type="email"
                className="form-control"
                name="email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
              {errors.email && <div className="error">{errors.email}</div>}
            </div>

            <div className="form-group">
              <label htmlFor="password">Password</label>
              <input
                type="password"
                className="form-control"
                name="password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />
              {errors.password && <div className="error">{errors.password}</div>}
            </div>
            <div className="form-group">
              <button type="submit" className="btn btn-primary btn-block">
                <span>Login</span>
              </button>
            </div>
          </form>
        </div>
      </div>
    </>
  );
};

Login.propTypes = {
  isWaiting: PropTypes.bool.isRequired,
  errorMsg: PropTypes.string.isRequired,
  login: PropTypes.func.isRequired,
  getUserProfileById: PropTypes.func.isRequired,
};

export default Login;
