import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import Tab from 'react-bootstrap/Tab';
import Tabs from 'react-bootstrap/Tabs';
import PostCreation from './PostCreation.jsx';
import Button from 'react-bootstrap/Button';
import '../../App.css';
import { toast } from 'react-toastify';
import Loading from '../../utils/Loading.jsx';
import PostManagementList from './PostManagementList.jsx';
import Pagination from 'react-bootstrap/Pagination';
import agent from '../../lib/agent.js';
import DropdownButton from 'react-bootstrap/DropdownButton';
import Dropdown from 'react-bootstrap/Dropdown';
import { Col, Form, Row } from 'react-bootstrap';
import { PostStatus } from '../../utils/utils.js';

function AdminHome(props) {
  const userInfo = useSelector((state) => state.auth.userInfo);
  const [key, setKey] = useState('published');
  const [isWaiting, setIsWaiting] = useState(true);
  const [errorMsg, setErrorMsg] = useState('');
  const [posts, setPosts] = useState([]);
  const [avatar, setAvatar] = useState('');
  const [modalShow, setModalShow] = useState(false);
  const [nReply, setNReply] = useState(''); // ASC DES
  const [nCreation, setNCreation] = useState('');
  const [creator, setCreator] = useState('');
  const [allPosts, setAllPosts] = useState([]);
  const [activePage, setActivePage] = useState(1);
  const [totalPage, setTotalPage] = useState(0);
  const [pageItem, setPageItem] = useState([]);
  const [adminMadeOpt, setAdminMadeOpt] = useState(false);
  const [dateFilterStatus, setDateFilterStatus] = useState('DESC');
  const [repliesFilterStatus, setRepliesFilterStatus] = useState('DESC');
  const [filterMode, setFilterMode] = useState('Default');

  const getUserProfileById = async () => {
    setErrorMsg('');

    const params = { userId: userInfo.userId };

    try {
      const result = await agent.User.getUserProfile(params);
      setAvatar(result.data.data.profileImageURL);

      return result.data;
    } catch (error) {
      setErrorMsg(error);
      return error;
    }
  };

  const handleGetUserPublishedPosts = async () => {
    setErrorMsg('');
    setIsWaiting(true);
    let postStus = 1;
    if (key === 'banned') {
      console.log('banned');
      postStus = PostStatus.Banned
      // postResponse = await agent.Post.getAllTypePosts(PostStatus.Banned, activePage - 1, 6);
    } else if (key === 'deleted') {
      console.log('deleted');
      postStus = PostStatus.Deleted
      // postResponse = await agent.Post.getAllTypePosts(PostStatus.Deleted, activePage - 1, 6);
    } else {
      console.log('published');
      postStus = PostStatus.Published
      // postResponse = await agent.Post.getAllTypePosts(PostStatus.Published, activePage - 1, 6);
    }

    try {
      setAdminMadeOpt(false);
      let postResponse = null;
      if (filterMode === 'Default') {
        postResponse = await agent.Post.getAllTypedPostsWithPagesSortByDate(postStus, activePage - 1, 6, 'DESC');
      } else if (filterMode === 'Date') {
        postResponse = await agent.Post.getAllTypedPostsWithPagesSortByDate(postStus, activePage - 1, 6, dateFilterStatus);
      } else if (filterMode === 'Replies') {
        console.log(repliesFilterStatus);
        postResponse = await agent.Post.getAllTypedPostWithPagesSortByReplies(postStus,
            activePage - 1,
            6,
            repliesFilterStatus,
        );
      } else if (filterMode === 'Name') {
        postResponse = await agent.Composite.getAllTypedPostsWithPagesFilterByName(postStus, activePage - 1, 6, creator);
      }

      // console.log(postResponse)
      if (postResponse !== null && postResponse.statusText === 'OK') {
        console.log('okk');
        console.log(postResponse);

        const pages = postResponse.data.data.totalPages;
        setTotalPage(pages);
        let i = 0;
        const rows = [];
        while (++i <= pages) rows.push(i);
        setPageItem(rows);

        const curPageData = postResponse.data.data.posts;

        for (let i = 0; i < curPageData.length; i++) {
          try {
            const post = curPageData.at(i);
            const res = await agent.User.getUserGeneralInfoById(post.userId);
            post.username = res.data.data.firstName + ' ' + res.data.data.lastName;
          } catch (error) {
            setIsWaiting(false);
            setErrorMsg(error);
          }
        }
        setPosts(curPageData);
        setAllPosts(curPageData);
        // console.log(posts)
      } else {
        setIsWaiting(false);
        toast.error(postResponse.message);
      }
    } catch (error) {
      setIsWaiting(false);
      toast.error(error);
    }
    setAdminMadeOpt(false);
    setIsWaiting(false);
  };
  
  const handleDateFilter = async () => {
    try {
      const newFilterStatus = dateFilterStatus === 'DESC' ? 'ASC' : 'DESC';
      setDateFilterStatus(newFilterStatus);
      setFilterMode('Date');
    } catch (error) {
      console.error('Error sorting Posts by dates:', error);
    }
  };

  const handleRepliesFilter = async () => {
    try {
      const newFilterStatus = repliesFilterStatus === 'DESC' ? 'ASC' : 'DESC';
      setRepliesFilterStatus(newFilterStatus);
      setFilterMode('Replies');
    } catch (error) {
      console.error('Error sorting Posts by Replies:', error);
    }
  };

  const handleNameFilter = async () => {
    console.log(creator);
    try {
      setFilterMode('Name');
    } catch (error) {
      console.error('Error Filtering Posts By Name:', error);
    }
  };

  const resetFilter = () => {
    setRepliesFilterStatus('DESC');
    setDateFilterStatus('DESC');
    setCreator('');
    setFilterMode('Default');
    // button empty
    setPosts([...allPosts]);
    console.log(allPosts);
  };

  const handleAdminMadeOpt = () => {
    setAdminMadeOpt(true);
  };

  useEffect(() => {
    getUserProfileById();
    handleGetUserPublishedPosts();
    setCreator('');
  }, [window.location, activePage, key]);

  useEffect(() => {
    handleGetUserPublishedPosts();
  }, [window.location, activePage, dateFilterStatus, repliesFilterStatus, filterMode, adminMadeOpt]);

  return (
    <>
      <PostCreation show={modalShow} onHide={() => setModalShow(false)} />
      <div className="container-fluid mt-5">
        <div className="userhomeInfo">
          <PostCreation show={modalShow} onHide={() => setModalShow(false)} />
          <img className="userhomeAvatar" src={avatar} style={{ width: '120px', height: '120px' }} />
          <p className="mt-2" style={{ fontSize: '24px' }}>
            <strong>
              {userInfo.firstName} {userInfo.lastName}
            </strong>
          </p>
        </div>

        <div>
          <Button variant="warning" className="newPostBtn" onClick={() => setModalShow(true)}>
            Create New Post
          </Button>{' '}
        </div>

        <Tabs id="controlled-tab-example" activeKey={key} onSelect={(k) => setKey(k)} className="mb-3">
          <Tab eventKey="published" title="Published">


            <div className="BtnContainer">
              <div className="btnItem">
                <button type="button" className="btn btn-primary" onClick={handleDateFilter}>
                  {dateFilterStatus === 'DESC' ? 'Sort By Date Created (Desc)' : 'Sort By Date Created (Asc)'}
                </button>
              </div>{' '}
              <div className="btnItem">
                <button type="button" className="btn btn-primary" onClick={handleRepliesFilter}>
                  {repliesFilterStatus === 'DESC' ? 'Sort By Replies No. (Desc)' : 'Sort By Replies No. (Asc)'}
                </button>
              </div>{' '}
              <div className="item btnItem">
                <Row>
                  <Col sm={4}>
                    <Form className="d-flex">
                      <Form.Control
                          type="search"
                          placeholder="Creator"
                          className="me-2 rounded-pill"
                          value={creator || ''}
                          aria-label="Search"
                          onChange={(e) => setCreator(e.target.value)}
                      />
                      <Button
                          className="rounded-pill"
                          variant="outline-primary"
                          onClick={handleNameFilter}
                          style={{ marginLeft: '5px' }}
                      >
                        Apply
                      </Button>
                    </Form>
                  </Col>
                </Row>
              </div>
              <div className="btnItem">
                <Button variant="outline-primary" onClick={resetFilter}>
                  Reset Filters
                </Button>{' '}
              </div>
            </div>




            {isWaiting ? (
              <Loading />
            ) : (
              <div className="col-md-12">
                <div>
                  <PostManagementList
                    posts={posts}
                    isWaiting={isWaiting}
                    isDeleted={false}
                    toSetAdminMadeOpt={handleAdminMadeOpt}
                  />
                </div>
                <div>
                  <Pagination>
                    {pageItem.map(function (i) {
                      return (
                        <Pagination.Item key={i} active={i === activePage} onClick={(e) => setActivePage(i)}>
                          {i}
                        </Pagination.Item>
                      );
                    })}
                  </Pagination>
                </div>
              </div>
            )}
          </Tab>

          <Tab eventKey="banned" title="Banned">


            <div className="BtnContainer">
              <div className="btnItem">
                <button type="button" className="btn btn-primary" onClick={handleDateFilter}>
                  {dateFilterStatus === 'DESC' ? 'Sort By Date Created (Desc)' : 'Sort By Date Created (Asc)'}
                </button>
              </div>{' '}
              <div className="btnItem">
                <button type="button" className="btn btn-primary" onClick={handleRepliesFilter}>
                  {repliesFilterStatus === 'DESC' ? 'Sort By Replies No. (Desc)' : 'Sort By Replies No. (Asc)'}
                </button>
              </div>{' '}
              <div className="item btnItem">
                <Row>
                  <Col sm={4}>
                    <Form className="d-flex">
                      <Form.Control
                          type="search"
                          placeholder="Creator"
                          className="me-2 rounded-pill"
                          value={creator || ''}
                          aria-label="Search"
                          onChange={(e) => setCreator(e.target.value)}
                      />
                      <Button
                          className="rounded-pill"
                          variant="outline-primary"
                          onClick={handleNameFilter}
                          style={{ marginLeft: '5px' }}
                      >
                        Apply
                      </Button>
                    </Form>
                  </Col>
                </Row>
              </div>
              <div className="btnItem">
                <Button variant="outline-primary" onClick={resetFilter}>
                  Reset Filters
                </Button>{' '}
              </div>
            </div>





            {isWaiting ? (
              <Loading />
            ) : (
              <div className="col-md-12">
                <div>
                  <PostManagementList
                    posts={posts}
                    isWaiting={isWaiting}
                    isDeleted={false}
                    toSetAdminMadeOpt={handleAdminMadeOpt}
                  />
                </div>
                <div>
                  <Pagination>
                    {pageItem.map(function (i) {
                      return (
                        <Pagination.Item key={i} active={i === activePage} onClick={(e) => setActivePage(i)}>
                          {i}
                        </Pagination.Item>
                      );
                    })}
                  </Pagination>
                </div>
              </div>
            )}
          </Tab>

          <Tab eventKey="deleted" title="Deleted">


            <div className="BtnContainer">
              <div className="btnItem">
                <button type="button" className="btn btn-primary" onClick={handleDateFilter}>
                  {dateFilterStatus === 'DESC' ? 'Sort By Date Created (Desc)' : 'Sort By Date Created (Asc)'}
                </button>
              </div>{' '}
              <div className="btnItem">
                <button type="button" className="btn btn-primary" onClick={handleRepliesFilter}>
                  {repliesFilterStatus === 'DESC' ? 'Sort By Replies No. (Desc)' : 'Sort By Replies No. (Asc)'}
                </button>
              </div>{' '}
              <div className="item btnItem">
                <Row>
                  <Col sm={4}>
                    <Form className="d-flex">
                      <Form.Control
                          type="search"
                          placeholder="Creator"
                          className="me-2 rounded-pill"
                          value={creator || ''}
                          aria-label="Search"
                          onChange={(e) => setCreator(e.target.value)}
                      />
                      <Button
                          className="rounded-pill"
                          variant="outline-primary"
                          onClick={handleNameFilter}
                          style={{ marginLeft: '5px' }}
                      >
                        Apply
                      </Button>
                    </Form>
                  </Col>
                </Row>
              </div>
              <div className="btnItem">
                <Button variant="outline-primary" onClick={resetFilter}>
                  Reset Filters
                </Button>{' '}
              </div>
            </div>




            {isWaiting ? (
              <Loading />
            ) : (
              <div className="col-md-12">
                <div>
                  <PostManagementList
                    posts={posts}
                    isWaiting={isWaiting}
                    isDeleted={true}
                    toSetAdminMadeOpt={handleAdminMadeOpt}
                  />
                </div>
                <div>
                  <Pagination>
                    {pageItem.map(function (i) {
                      return (
                        <Pagination.Item key={i} active={i === activePage} onClick={(e) => setActivePage(i)}>
                          {i}
                        </Pagination.Item>
                      );
                    })}
                  </Pagination>
                </div>
              </div>
            )}
          </Tab>
        </Tabs>
      </div>
    </>
  );
}

AdminHome.protoTypes = {};

export default AdminHome;
