import PropTypes, { instanceOf } from 'prop-types';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import agent from '../../lib/agent.js';
import { toast } from 'react-toastify';
import '../../App.css';
import { useNavigate } from 'react-router-dom';
import { PostStatus, PostStatusStr, dateParser } from '../../utils/utils.js';
import LoadingModal from '../Common/LoadingModal.jsx';
import Loading from '../../utils/Loading.jsx';
import Pagination from 'react-bootstrap/Pagination';
import React from 'react';
import Badge from 'react-bootstrap/Badge';
import Stack from 'react-bootstrap/Stack';

function PostManagementList(props) {
  const { posts, isDeleted, isWaiting, toSetAdminMadeOpt } = props;
  const navigate = useNavigate();

  // console.log(posts);

  const handleClick = async (postId) => {
    // update history
    try {
      const postResponse = await agent.Post.updateHistory({ postId: postId });

      if (postResponse.statusText === 'OK') {
        // setRedirect(true);

        // redirect to post detail
        // window.location.href = '/posts';
        navigate(`/posts/${postId}`);
      } else {
        console.log(postResponse.message);
        toast.error(postResponse.message);
      }
    } catch (error) {
      toast.error(error);
    }
  };

  const handleBanUnban = async (toStauts, postId) => {
    if (toStauts === 'ban') {
      try {
        const postResponse = await agent.Post.banPost(postId);

        if (postResponse.statusText === 'OK') {
          console.log(postResponse);
          // navigate(`/posts/${postId}`);
        } else {
          console.log(postResponse.message);
          toast.error(postResponse.message);
        }
        toSetAdminMadeOpt();
      } catch (error) {
        toast.error(error);
        toSetAdminMadeOpt();
      }
    } else if (toStauts === 'unban') {
      try {
        const postResponse = await agent.Post.publishPost(postId);

        if (postResponse.statusText === 'OK') {
          console.log(postResponse);
          // navigate(`/posts/${postId}`);
        } else {
          console.log(postResponse.message);
          toast.error(postResponse.message);
        }
        toSetAdminMadeOpt();
      } catch (error) {
        toast.error(error);
        toSetAdminMadeOpt();
      }
    }
  };
  const handleRecover = async (postId) => {
    try {
      const postResponse = await agent.Post.recoverPost(postId);

      if (postResponse.statusText === 'OK') {
        console.log(postResponse);
        // navigate(`/posts/${postId}`);
      } else {
        console.log(postResponse.message);
        toast.error(postResponse.message);
      }
      toSetAdminMadeOpt();
    } catch (error) {
      toast.error(error);
      toSetAdminMadeOpt();
    }
  };

  return (
    <Row xs={1} md={3} className="g-4">
      {posts.map((post) => {
        const { postId, dateCreated, title, username, status } = post;
        return (
          <Col key={postId}>
            <Card>
              <Card.Header className="d-flex" style={{ justifyContent: 'space-between' }}>
                <div>{username}</div>{' '}
                <div>
                  {status === PostStatusStr.Published && (
                    <Badge bg="primary" style={{ color: 'white' }}>
                      {status}
                    </Badge>
                  )}
                  {status === PostStatusStr.Unpublished && (
                    <Badge bg="info" style={{ color: 'white' }}>
                      {status}
                    </Badge>
                  )}
                  {status === PostStatusStr.Hidden && (
                    <Badge bg="secondary" style={{ color: 'white' }}>
                      {status}
                    </Badge>
                  )}
                  {status === PostStatusStr.Banned && (
                    <Badge bg="warning" style={{ color: 'white' }}>
                      {status}
                    </Badge>
                  )}
                  {status === PostStatusStr.Deleted && (
                    <Badge bg="danger" style={{ color: 'white' }}>
                      {status}
                    </Badge>
                  )}
                </div>
              </Card.Header>

              <Card.Body>
                <Card.Title>
                  <span className="card-title">{title}</span>
                </Card.Title>
                <Card.Text>{dateParser(dateCreated)}</Card.Text>
                <Button variant="primary" onClick={() => handleClick(postId)}>
                  Post Detail
                </Button>{' '}
                {isDeleted ? (
                  <Button variant="warning" onClick={() => handleRecover(postId)}>
                    Recover
                  </Button>
                ) : (
                  <>
                    {post.status === PostStatusStr.Banned ? (
                      <Button
                        variant="success"
                        onClick={() => {
                          handleBanUnban('unban', postId);
                          toSetAdminMadeOpt();
                        }}
                        disabled={status === 'Published'}
                        style={{ marginRight: '4px' }}
                      >
                        Unban
                      </Button>
                    ) : (
                      <Button
                        variant="danger"
                        onClick={() => {
                          handleBanUnban('ban', postId);
                          toSetAdminMadeOpt();
                        }}
                        disabled={status === 'Banned'}
                      >
                        Ban
                      </Button>
                    )}
                  </>
                )}
              </Card.Body>
            </Card>
          </Col>
        );
      })}
    </Row>
  );
}

PostManagementList.propTypes = {
  posts: PropTypes.arrayOf(
    PropTypes.shape({
      userId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
      dateCreated: PropTypes.oneOfType([PropTypes.string, instanceOf(Date)]),
      title: PropTypes.string,
      postId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
      username: PropTypes.string.isRequired,
    }),
  ).isRequired,
  isWaiting: PropTypes.bool.isRequired,
  isDeleted: PropTypes.bool.isRequired,
  handleAdminMadeOpt: PropTypes.func.isRequired,
};
PostManagementList.defaultProps = {
  posts: [],
  redirect: false,
  isDeleted: false,
};
export default PostManagementList;
