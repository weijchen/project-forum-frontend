import React, { useState } from 'react';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import PropTypes, {bool} from 'prop-types';
import axios from "axios";
import agent from "../../lib/agent.js";
import { toast } from 'react-toastify';
import '../../css/home.css'

function PostCreation(props) {
    const {
        // isWaiting,
        // errorMsg,
        show,
        onHide
    } = props;
    const [selectedFiles, setSelectedFiles] = useState([]);
    const [title, setTitle] = useState('');
    const [content, setContent] = useState('');
    const [titleValid, setTitleValid] = useState(true);

    const onFileChange = event => {
        // Update the state

        let sf = selectedFiles
        let curFile = event.target.files[0];
        let isSet = true;
        for(let i = 0; i<selectedFiles.length; ++i) {
            if(isSet === false) break
            if(selectedFiles.at(i).name === curFile.name) {
                toast.error("redundant file name");
                isSet = false;
            }
        }
        if(isSet === true) {
            sf.push(event.target.files[0])
            setSelectedFiles(sf);
        }

    };


    const onFileUpload = () => {

        // Create an object of formData
        // const formData = new FormData();
        // let newFile = selectedFiles.at(selectedFiles.length-1)

        let newArray = selectedFiles.slice();
        setSelectedFiles(newArray);
    };
    const handleDelete = (filename) => {
        // console.log(filename);

        let  newSelectedFiles = selectedFiles.filter(function(item) {
            return item.name !== filename
        })
        let nsf = [...newSelectedFiles];
        // console.log(nsf)
        setSelectedFiles(nsf);
    }

    const handlePost = async (status) => {
        if (!title || !title.trim()) {
            toast.error("Title is required");
            return;
        }

        const formData = new FormData();

        // Append non-file data
        formData.append("title", title);
        formData.append("content", content);
        formData.append("status", status);

        const selectedImages = [];
        const selectedApplications = [];

        for(let i = 0; i < selectedFiles.length; ++i) {
            let f = selectedFiles.at(i);
            // console.log(f);
            if(f.size >= 1048576) {
                toast.error("The file size exceeds its maximum permitted size of 1048576 bytes.");
                return;
            }
            let tp = f.type.split('/').at(0);
            console.log(tp);
            // if(tp === 'image') {
            //     selectedImages.push(f)
            // } else {
            //     selectedApplications.push(f);
            // }
            // console.log(f.size);
        }

        const params = {
            title: title,
            content: content,
            status: status,
            images: selectedImages,
            files: selectedApplications
        };



        // Update the formData object by appending each file
        for (let i = 0; i < selectedFiles.length; i++) {
            let file = selectedFiles[i];
            let tp = file.type.split('/').at(0);

            if (tp === 'image') {
                formData.append("images", file, file.name);
            } else {
                formData.append("files", file, file.name);
            }
        }

        console.log(formData);
        try {
            const result = await agent.Composite.newPost(formData);

            toast.success(result.data.message);

            setSelectedFiles([]);
            setTitle('');
            setContent('');
            window.location.reload();
        } catch (error) {
            // setIsWaiting(false);
            console.log('err:');
            toast.error(error);
            return error;
        }
    }

    const handleTitleChange = (e) => {
        setTitle(e.target.value);
        setTitleValid(e.target.value.trim() !== "");
    }

    return (
        <>
            <Modal {...props} size="lg" aria-labelledby="contained-modal-title-vcenter" centered>
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">New Post</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form>
                        <Form.Group controlId="formBasicTitle">
                            <Form.Label>Title</Form.Label>
                            <Form.Control type="text" placeholder="title" onChange={handleTitleChange} isInvalid={!titleValid} />
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
                            <Form.Label>Content</Form.Label>
                            <Form.Control as="textarea" rows={3} placeholder="share yout thoughts" onChange={(e) => setContent(e.target.value)} />
                        </Form.Group>
                    </Form>
                    <p>
                        Add files and images
                    </p>
                    <div>

                        <input type="file" onChange={onFileChange} />
                        <button onClick={onFileUpload}>
                            Upload
                        </button>
                    </div>
                    <div className="uploaded-files-list">
                        {selectedFiles.map(file => (

                            <div key={file.name}>
                                {file.name}
                                {(file.size >= 1048576) ? (<span className="redFontSpan"> file size is too large</span>):(<span></span>)}
                                <button onClick={()=>handleDelete(file.name)}>delete</button>
                            </div>
                        ))}
                    </div>
                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={() => handlePost('Published')}>Post</Button>
                    <Button onClick={() => handlePost('Unpublished')}>Save as draft</Button>
                </Modal.Footer>
            </Modal>
        </>
    );
}

PostCreation.propTypes = {
    // isWaiting: PropTypes.bool.isRequired,
    // errorMsg: PropTypes.object.isRequired,
};

export default PostCreation;
