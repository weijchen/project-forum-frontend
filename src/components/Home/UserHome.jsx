import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { toast } from 'react-toastify';
import PropTypes from 'prop-types';
import PostList from './PostList.jsx';
import agent from '../../lib/agent';
import Button from 'react-bootstrap/Button';
import '../../App.css';
import PostCreation from './PostCreation.jsx';
import Loading from '../../utils/Loading.jsx';
import { Col, Form, Row } from 'react-bootstrap';
import Pagination from 'react-bootstrap/Pagination';
import { USER_TYPE } from '../../constants/userType.js';

function UserHome(props) {
  const [isWaiting, setIsWaiting] = useState(true);
  const [errorMsg, setErrorMsg] = useState('');
  const [posts, setPosts] = useState([]);
  const [avatar, setAvatar] = useState('');
  const [modalShow, setModalShow] = useState(false);
  const [nReply, setNReply] = useState(''); // ASC DES
  const [nCreation, setNCreation] = useState('');
  const [creator, setCreator] = useState('');
  const [allPosts, setAllPosts] = useState([]);
  const [activePage, setActivePage] = useState(1);
  const [totalPage, setTotalPage] = useState(0);
  const [pageItem, setPageItem] = useState([]);
  const [dateFilterStatus, setDateFilterStatus] = useState('DESC');
  const [repliesFilterStatus, setRepliesFilterStatus] = useState('DESC');
  const [filterMode, setFilterMode] = useState('Default');
  const [madeOpt, setMadeOpt] = useState(false);

  const userInfo = useSelector((state) => state.auth.userInfo);

  const getUserProfileById = async () => {
    setErrorMsg('');
    const params = { userId: userInfo.userId };
    try {
      const result = await agent.User.getUserProfile(params);
      setAvatar(result.data.data.profileImageURL);

      return result.data;
    } catch (error) {
      setErrorMsg(error);
      return error;
    }
  };

  const handleGetUserPublishedPosts = async () => {
    setErrorMsg('');
    setIsWaiting(true);
    try {
      setMadeOpt(false);
      let postResponse;
      console.log('FilterMode: ' + filterMode);
      if (filterMode === 'Default') {
        postResponse = await agent.Post.getAllPublishedPostsWithPagesSortByDate(activePage - 1, 6, 'DESC');
      } else if (filterMode === 'Date') {
        postResponse = await agent.Post.getAllPublishedPostsWithPagesSortByDate(activePage - 1, 6, dateFilterStatus);
      } else if (filterMode === 'Replies') {
        postResponse = await agent.Post.getAllPublishedPostWithPagesSortByReplies(
          activePage - 1,
          6,
          repliesFilterStatus,
        );
      } else if (filterMode === 'Name') {
        postResponse = await agent.Composite.getAllPublishedPostsWithPagesFilterByName(activePage - 1, 6, creator);
      }
      console.log(postResponse);
      if (postResponse.statusText === 'OK') {
        const pages = postResponse.data.data.totalPages;
        setTotalPage(pages);
        let i = 0;
        const rows = [];
        while (++i <= pages) rows.push(i);
        setPageItem(rows);
        const curPageData = postResponse.data.data.posts;

        // get page information
        for (let i = 0; i < curPageData.length; i++) {
          try {
            const post = curPageData.at(i);
            const res = await agent.User.getUserGeneralInfoById(post.userId);
            post.username = 'default';
            post.username = res.data.data.firstName + ' ' + res.data.data.lastName;
          } catch (error) {
            setIsWaiting(false);
            setErrorMsg(error);
          }
        }

        setPosts(curPageData);
        setAllPosts(curPageData);
      } else {
        setIsWaiting(false);
        toast.error(postResponse.message);
      }
    } catch (error) {
      setIsWaiting(false);
      toast.error(error);
    }
    setMadeOpt(false);
    setIsWaiting(false);
  };

  const handleDateFilter = async () => {
    try {
      const newFilterStatus = dateFilterStatus === 'DESC' ? 'ASC' : 'DESC';
      setDateFilterStatus(newFilterStatus);
      setFilterMode('Date');
    } catch (error) {
      console.error('Error sorting Posts by dates:', error);
    }
  };

  const handleRepliesFilter = async () => {
    try {
      const newFilterStatus = repliesFilterStatus === 'DESC' ? 'ASC' : 'DESC';
      setRepliesFilterStatus(newFilterStatus);
      setFilterMode('Replies');
    } catch (error) {
      console.error('Error sorting Posts by Replies:', error);
    }
  };

  const handleNameFilter = async () => {
    console.log(creator);
    try {
      setFilterMode('Name');
      setActivePage(1);
    } catch (error) {
      console.error('Error Filtering Posts By Name:', error);
    }
  };

  const resetFilter = () => {
    setRepliesFilterStatus('DESC');
    setDateFilterStatus('DESC');
    setCreator('');
    setFilterMode('Default');
    // button empty
    setPosts([...allPosts]);
    console.log(allPosts);
  };

  const handleMadeOpt = () => {
    setMadeOpt(true);
  };

  useEffect(() => {
    getUserProfileById();
  }, [window.location]);

  useEffect(() => {
    console.log('date Filter status');
    console.log(dateFilterStatus);
    handleGetUserPublishedPosts();
  }, [window.location, activePage, dateFilterStatus, repliesFilterStatus, filterMode, madeOpt]);

  return (
    <>
      {/*<LoadingModal isWaiting={isWaiting} errorMsg={errorMsg} />*/}
      <PostCreation show={modalShow} onHide={() => setModalShow(false)} />

      <div className="container-fluid mt-5">
        <div className="userhomeInfo">
          {/*<PostCreation show={modalShow} onHide={() => setModalShow(false)} />*/}
          <img className="userhomeAvatar" src={avatar} style={{ width: '120px', height: '120px' }} />
          <p className="mt-2" style={{ fontSize: '24px' }}>
            <strong>
              {userInfo.firstName} {userInfo.lastName}
            </strong>
          </p>
        </div>

        <div>
          {userInfo.role !== USER_TYPE.NORMAL_USER_NOT_VALID && userInfo.role !== USER_TYPE.VISITOR_BANNED && (
            <Button variant="warning" className="newPostBtn" onClick={() => setModalShow(true)}>
              Create New Post
            </Button>
          )}
        </div>

        <div className="BtnContainer">
          <div className="btnItem">
            <button type="button" className="btn btn-primary" onClick={handleDateFilter}>
              {dateFilterStatus === 'DESC' ? 'Sort By Date Created (Desc)' : 'Sort By Date Created (Asc)'}
            </button>
          </div>{' '}
          <div className="btnItem">
            <button type="button" className="btn btn-primary" onClick={handleRepliesFilter}>
              {repliesFilterStatus === 'DESC' ? 'Sort By Replies No. (Desc)' : 'Sort By Replies No. (Asc)'}
            </button>
          </div>{' '}
          <div className="item btnItem">
            <Row>
              <Col sm={4}>
                <Form className="d-flex">
                  <Form.Control
                    type="search"
                    placeholder="Creator"
                    className="me-2 rounded-pill"
                    value={creator || ''}
                    aria-label="Search"
                    onChange={(e) => setCreator(e.target.value)}
                  />
                  <Button
                    className="rounded-pill"
                    variant="outline-primary"
                    onClick={handleNameFilter}
                    style={{ marginLeft: '5px' }}
                  >
                    Apply
                  </Button>
                </Form>
              </Col>
            </Row>
          </div>
          <div className="btnItem">
            <Button variant="outline-primary" onClick={resetFilter}>
              Reset Filters
            </Button>{' '}
          </div>
        </div>




        {isWaiting ? (
          <Loading />
        ) : (
          <>
            <div className="col-md-12">
              <div>
                <PostList posts={posts} isWaiting={isWaiting} curUserId={userInfo.userId} handleMadeOpt={handleMadeOpt}/>
              </div>
              <div>
                <Pagination>
                  {pageItem.map(function (i) {
                    return (
                      <Pagination.Item key={i} active={i === activePage} onClick={(e) => setActivePage(i)}>
                        {i}
                      </Pagination.Item>
                    );
                  })}
                </Pagination>
              </div>
            </div>
          </>
        )}
      </div>
    </>
  );
}

UserHome.protoTypes = {
  isWaiting: PropTypes.bool.isRequired,
  errorMsg: PropTypes.string.isRequired,
  getUserPublishedPosts: PropTypes.func.isRequired,
};

export default UserHome;
