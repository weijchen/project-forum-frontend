import PropTypes, { instanceOf } from 'prop-types';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import agent from '../../lib/agent.js';
import { toast } from 'react-toastify';
import '../../App.css';
import { useNavigate } from 'react-router-dom';
import { dateParser, PostStatusStr } from '../../utils/utils.js';
import Badge from 'react-bootstrap/Badge';
import React from 'react';

const PostList = ({ posts, curUserId, handleMadeOpt }) => {
  const navigate = useNavigate();

  // if (!posts.length) {
  //   // return <LoadingModal isWaiting={true} errorMsg={null} />;
  //   return <p>Loading...</p>;
  // }

  const handleDelete = async (postId) => {
    try {
      const postResponse = await agent.Post.deletePost(postId);

      if (postResponse.statusText === 'OK') {
        console.log(postResponse);

        toast.success("Successfully delete!");
        window.location.reload();
      } else {
        console.log(postResponse.message);
        toast.error(postResponse.message);
      }
      handleMadeOpt();
    } catch (error) {
      toast.error(error);
      handleMadeOpt();
    }
  };

  const handleClick = async (postId) => {
    // update history
    try {
      const postResponse = await agent.Post.updateHistory({ postId: postId });

      if (postResponse.statusText === 'OK') {
        // setRedirect(true);

        // redirect to post detail
        // window.location.href = '/posts';
        navigate(`/posts/${postId}`);
      } else {
        console.log(postResponse.message);
        toast.error(postResponse.message);
      }
    } catch (error) {
      toast.error(error);
    }
  };

  return (
    <Row xs={1} md={3} className="g-4">
      {posts.map((post) => {
        const { postId, dateCreated, title, username, status, userId } = post;
        return (
          <Col key={postId}>
            <Card>
              <Card.Header className="d-flex" style={{ justifyContent: 'space-between' }}>
                <div>{username}</div>{' '}
                <div>
                  {status === PostStatusStr.Published && (
                    <Badge bg="primary" style={{ color: 'white' }}>
                      {status}
                    </Badge>
                  )}
                  {status === PostStatusStr.Unpublished && (
                    <Badge bg="info" style={{ color: 'white' }}>
                      {status}
                    </Badge>
                  )}
                  {status === PostStatusStr.Hidden && (
                    <Badge bg="secondary" style={{ color: 'white' }}>
                      {status}
                    </Badge>
                  )}
                  {status === PostStatusStr.Banned && (
                    <Badge bg="warning" style={{ color: 'white' }}>
                      {status}
                    </Badge>
                  )}
                  {status === PostStatusStr.Deleted && (
                    <Badge bg="danger" style={{ color: 'white' }}>
                      {status}
                    </Badge>
                  )}
                </div>
              </Card.Header>
              <Card.Body>
                <Card.Title>
                  <span className="card-title">{title}</span>
                </Card.Title>
                <Card.Text>{dateParser(dateCreated)}</Card.Text>
                <Button variant="primary" onClick={() => handleClick(postId)} size="sm">
                  Post Detail
                </Button>{' '}
                {userId == curUserId && (
                  <Button
                    variant="warning"
                    onClick={() => handleDelete(postId)}
                    disabled={userId !== curUserId}
                    size="sm"
                  >
                    Delete Post
                  </Button>
                )}
              </Card.Body>
            </Card>
          </Col>
        );
      })}
    </Row>
  );
};

PostList.propTypes = {
  posts: PropTypes.arrayOf(
    PropTypes.shape({
      userId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
      dateCreated: PropTypes.oneOfType([PropTypes.string, instanceOf(Date)]),
      title: PropTypes.string,
      postId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
      username: PropTypes.string.isRequired,
    }),
  ).isRequired,
  isWaiting: PropTypes.bool.isRequired,
  curUserId: PropTypes.number.isRequired,
};
PostList.defaultProps = {
  posts: [],
  redirect: false,
  curUserId: -1,
};
export default PostList;
