import '../../css/post.css';
import React, { useEffect, useState } from 'react';
import { Card, Button, ListGroup, ListGroupItem, Form, FormControl } from 'react-bootstrap';
import Modal from 'react-bootstrap/Modal';
import axios from 'axios';
import { useSelector } from 'react-redux';
import { toast } from 'react-toastify';
import { useLocation } from 'react-router';
import agent from '../../lib/agent';
import LoadingModal from '../Common/LoadingModal.jsx';
import { useParams } from 'react-router-dom';
import { USER_TYPE_INT, USER_TYPE } from '../../constants/userType';
import { PostStatusStr } from '../../utils/utils';
import Badge from 'react-bootstrap/Badge';

function PostDetail(props) {
  const userInfo = useSelector((state) => state.auth.userInfo);
  const [post, setPost] = useState({});
  const location = useLocation();
  const { postId } = useParams();

  const [replyComment, setReplyComment] = useState({ comment: '' });
  const [subreplyComment, setSubreplyComment] = useState([]);
  const [replySubmitted, setReplySubmitted] = useState(false);
  const [title, setTitle] = useState('');
  const [content, setContent] = useState('');
  const [modalShow, setModalShow] = useState(false);
  const [isWaiting, setIsWaiting] = useState(false);
  const [errorMsg, setErrorMsg] = useState('');
  const [edited, setEdited] = useState(false);
  const [editTime, setEditTime] = useState("");

  const handleGetPostByPostId = async (id) => {
    setErrorMsg('');
    setIsWaiting(true);
    try {
      const postresponse = await agent.Post.getPostByPostId(id);
      if (postresponse.data.success) {
        const postData = postresponse.data.data;
        setPost(postData);
        const initSubreplyComment = postresponse.data.data.postReplies.map(() => ({ comment: '' }));
        setSubreplyComment(initSubreplyComment);
        setTitle(postresponse.data.data.title);
        setContent(postresponse.data.data.content);
        setIsWaiting(false);
        if(postData.dateModified) {
          setEdited(true);
          setEditTime(postData.dateModified);
        }
      } else {
        setIsWaiting(false);
        setErrorMsg('');
        toast.error(postresponse.data.message);
      }
    } catch (error) {
      setIsWaiting(false);
      toast.error(error);
    }
  };

  const handleGetFile = async (event, id) => {
    setIsWaiting(true);
    try {
      const response = await agent.Post.getFileById(id);
      setIsWaiting(false);
      window.location = response.data;
    } catch (error) {
      setIsWaiting(false);
      toast.error(error);
    }
  };

  const handleReplySubmit = async (event, id) => {
    event.preventDefault();
    console.log('create reply: ' + id);
    setIsWaiting(true);
    try {
      const response = await agent.Post.createReply(id, replyComment);
      toast.success('Successfully post reply');
      setReplySubmitted(!replySubmitted);
      setIsWaiting(false);
      const cacheResponse = await agent.Post.updateCache(postId);
      console.log(response.data);
    } catch (error) {
      setIsWaiting(false);
      toast.error(error);
    }
    setReplyComment({ comment: '' });
  };

  const handleInputChange = (event) => {
    setReplyComment({ comment: event.target.value });
  };

  const handleSubreplySubmit = async (event, id, idx) => {
    event.preventDefault();
    setIsWaiting(true);
    console.log('create subreply: ' + id);
    try {
      const response = await agent.Post.createsubReply(id, idx + 1, subreplyComment[idx]);
      setReplySubmitted(!replySubmitted);
      const cacheResponse = await agent.Post.updateCache(postId);
      console.log('subreply: ' + response);
      if (response.data.success) {
        setIsWaiting(false);
      }
    } catch (error) {
      setIsWaiting(false);
      toast.error(error);
    }
    setSubreplyComment((prevState) => {
      const newState = [...prevState];
      newState[idx] = { comment: '' };
      return newState;
    });
  };

  const handleUpdatePost = async () => {
    setIsWaiting(true);
    try {
      const response = await agent.Post.updatePost(postId, { title, content });
      console.log(response.data);
      if (response.data.success) {
        setIsWaiting(false);
        setPost((prev) => ({
          ...prev,
          title: title,
          content: content,
        }));
        setModalShow(false); 
        setEditTime(new Date());
        setEdited(true);
        toast.success(response.data.message);
        const cacheResponse = await agent.Post.updateCache(postId);
        console.log(cacheResponse);
      } else {
        setIsWaiting(false);
        toast.error(response.data.message);
      }
    } catch (error) {
      setIsWaiting(false);
      toast.error(error);
    }
  };

  const handleSubReplyInputChange = (event, index) => {
    const newSubreplyComments = [...subreplyComment];
    newSubreplyComments[index] = { comment: event.target.value };
    setSubreplyComment(newSubreplyComments);
  };

  const handleDeleteReply = async (id, rpid) => {
    try {
      const postresponse = await agent.Post.deleteReply(id, rpid);
      console.log('handleDeleteReply' + postresponse);
      if (postresponse.data.success) {
        toast.success('successfully delete post reply');
        setReplySubmitted(!replySubmitted);
        const cacheResponse = await agent.Post.updateCache(postId);
      } else {
        toast.error(postresponse.data.message);
      }
    } catch (error) {
      toast.error(error);
    }
  };

  const handleDeleteSubReply = async (id, rpid, srpid) => {
    try {
      const postresponse = await agent.Post.deleteSubReply(id, rpid, srpid);
      console.log('delete subreply: ');
      console.log(postresponse);
      if (postresponse.data.success) {
        setReplySubmitted(!replySubmitted);
        const cacheResponse = await agent.Post.updateCache(postId);
      } else {
        toast.error(postresponse.data.message);
      }
    } catch (error) {
      toast.error(error);
    }
  };

  const handlePublishPost = async (id) => {
    setIsWaiting(true);
    try {
      const postresponse = await agent.Post.publishPost(id);
      console.log(postresponse.data);
      if (postresponse.data.success) {
        setIsWaiting(false);
        const cacheResponse = await agent.Post.updateCache(postId);
        console.log(cacheResponse);
        setReplySubmitted(!replySubmitted);
      } else {
        setIsWaiting(false);
        toast.error(postresponse.data.message);
      }
    } catch (error) {
      setIsWaiting(false);
      toast.error(error);
    }
  };

  const handleArchivePost = async (id) => {
    setIsWaiting(true);
    try {
      const postresponse = await agent.Post.archivePost(id);
      console.log(postresponse.data);
      if (postresponse.data.success) {
        setIsWaiting(false);
        const cacheResponse = await agent.Post.updateCache(postId);
        console.log(cacheResponse);
        setReplySubmitted(!replySubmitted);
      } else {
        setIsWaiting(false);
        toast.error(postresponse.data.message);
      }
    } catch (error) {
      setIsWaiting(false);
      toast.error(error);
    }
  };

  const handleHidePost = async (id) => {
    setIsWaiting(true);
    try {
      const postresponse = await agent.Post.hidePost(id);
      console.log(postresponse.data);
      if (postresponse.data.success) {
        setIsWaiting(false);
        const cacheResponse = await agent.Post.updateCache(postId);
        console.log(cacheResponse);
        setReplySubmitted(!replySubmitted);
      } else {
        setIsWaiting(false);
        toast.error(postresponse.data.message);
      }
    } catch (error) {
      setIsWaiting(false);
      toast.error(error);
    }
  };

  useEffect(() => {
    handleGetPostByPostId(postId);
  }, [location, replySubmitted]);

  return (
    <div className="p-3">
      <LoadingModal isWaiting={isWaiting} errorMsg={errorMsg} />
      <Modal size="lg" aria-labelledby="contained-modal-title-vcenter" centered show={modalShow}>
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">Update Post</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group controlId="formTitle">
              <Form.Label>Title</Form.Label>
              <Form.Control type="text" placeholder={title} onChange={(e) => setTitle(e.target.value)} />
            </Form.Group>
            <Form.Group controlId="formContent">
              <Form.Label>Content</Form.Label>
              <Form.Control type="text" placeholder={content} onChange={(e) => setContent(e.target.value)} />
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button type="button" onClick={handleUpdatePost}>
            Submit
          </Button>
          <Button onClick={() => setModalShow(false)}>Close</Button>
        </Modal.Footer>
      </Modal>

      {post && (
        <Card>
          <Card.Body>
            <div>
              {post.status === PostStatusStr.Published && (
                <Badge bg="primary" style={{ color: 'white' }}>
                  {post.status}
                </Badge>
              )}
              {post.status === PostStatusStr.Unpublished && (
                <Badge bg="info" style={{ color: 'white' }}>
                  {post.status}
                </Badge>
              )}
              {post.status === PostStatusStr.Hidden && (
                <Badge bg="secondary" style={{ color: 'white' }}>
                  {post.status}
                </Badge>
              )}
              {post.status === PostStatusStr.Banned && (
                <Badge bg="warning" style={{ color: 'white' }}>
                  {post.status}
                </Badge>
              )}
              {post.status === PostStatusStr.Deleted && (
                <Badge bg="danger" style={{ color: 'white' }}>
                  {post.status}
                </Badge>
              )}
              {post.isArchived && (
                <Badge bg="danger" style={{ color: 'white', marginLeft: '5px' }}>
                  Archived
                </Badge>
              )}
            </div>
            <Card.Title>
              <div style={{ display: 'flex', alignItems: 'center' }}>
                <h2>{post.title}</h2>
                {post.userId === userInfo.userId && (
                  <a href="#" style={{ paddingLeft: '20px' }} onClick={() => setModalShow(true)}>
                    {' '}
                    <p>[Edit Post]</p>
                  </a>
                )}
                {post.status === PostStatusStr.Unpublished && post.userId == userInfo.userId && (
                  <a href="#" style={{ paddingLeft: '20px' }} onClick={() => handlePublishPost(post.postId)}>
                    {' '}
                    <p>[Publish Post]</p>
                  </a>
                )}
                {post.status === PostStatusStr.Published && post.userId == userInfo.userId && (
                  <a href="#" style={{ paddingLeft: '20px' }} onClick={() => handleArchivePost(post.postId)}>
                    {' '}
                    {post.isArchived ? <p>[Enable Reply]</p> : <p>[Archive Post]</p>}
                  </a>
                )}
                {(post.status === PostStatusStr.Published || post.status === PostStatusStr.Hidden) &&
                  post.userId == userInfo.userId && (
                    <a
                      href="#"
                      style={{ paddingLeft: '20px' }}
                      onClick={() =>
                        post.status === PostStatusStr.Hidden
                          ? handlePublishPost(post.postId)
                          : handleHidePost(post.postId)
                      }
                    >
                      {' '}
                      {post.status === PostStatusStr.Hidden ? <p>[Reveal Post]</p> : <p>[Hide Post]</p>}
                    </a>
                  )}
              </div>
            </Card.Title>
            <Card.Text>
              <small className="text-muted">
                <span style={{ paddingLeft: '20px' }}>
                  <img
                    src={post.profileImageURL}
                    alt={`${post.firstName} ${post.lastName}`}
                    width="30"
                    height="30"
                    className="rounded-circle mr-2"
                  />
                  <strong>
                    {post.firstName} {post.lastName}
                  </strong>{' '}
                  on {new Date(post.dateCreated).toDateString()}
                  {edited && (
                    <span style={{ paddingLeft: '10px', fontStyle: 'italic' }}>
                      {' '}
                      Edit: {new Date(editTime).toDateString()}
                    </span>
                  )}
                </span>
              </small>
            </Card.Text>
            <Card.Body>
              <div style={{ fontSize: '20px', lineHeight: '1.5', color: '#333' }}>{post.content}</div>
              <br />
              <div style={{ paddingLeft: '20px' }}>
                {post.images &&
                  post.images.map((imageURL, index) => (
                    <React.Fragment key={index} >
                      <img src={imageURL} alt={`Post Image ${index}`} width="60" height="60" style={{ marginLeft: '15px', marginRight: '15px' }} />
                    </React.Fragment>
                  ))}
                {post.attachments &&
                  post.attachments.map((fileUrl, index) => (
                    <React.Fragment key={`file ${index}`}>
                      <br></br>
                      <strong style={{ color: '#6F7378', fontStyle: 'italic' }}>Attachment {index + 1}:</strong>
                      <a style={{ fontStyle: 'italic' }} href="#" onClick={(event) => handleGetFile(event, fileUrl)}>
                        {fileUrl}
                      </a>
                    </React.Fragment>
                  ))}
              </div>
            </Card.Body>
            {post.postReplies && post.postReplies.length > 0 && (
              <ListGroup className="mt-4">
                {post.postReplies.map(
                  (reply, index) =>
                    reply.active && (
                      <ListGroupItem key={index}>
                        <Card.Text>
                          <small className="text-muted">
                            <span
                              className="d-flex pb-3"
                              style={{ justifyContent: 'space-between', borderBottom: '1px solid lightgrey' }}
                            >
                              <span>
                                <img
                                  src={reply.profileImageURL}
                                  alt={`${reply.firstName} ${reply.lastName}`}
                                  width="30"
                                  height="30"
                                  className="rounded-circle mr-2"
                                />
                                {reply.firstName} {reply.lastName} on {new Date(reply.dateCreated).toDateString()}
                              </span>
                              {(reply.userId === userInfo.userId ||
                                userInfo.role === USER_TYPE.SUPER_ADMIN ||
                                userInfo.role === USER_TYPE.ADMIN) && (
                                <span>
                                  <Button
                                    variant="danger"
                                    type="submit"
                                    size="sm"
                                    onClick={(e) => handleDeleteReply(post.postId, index + 1)}
                                  >
                                    Delete
                                  </Button>
                                </span>
                              )}
                            </span>
                          </small>
                        </Card.Text>
                        {reply.comment}

                        {reply.subReplies && reply.subReplies.length > 0 && (
                          <ListGroup className="mt-2 ml-4">
                            {reply.subReplies.map(
                              (subreply, subIndex) =>
                                subreply.active && (
                                  <ListGroupItem key={`${index}-${subIndex}`} style={{ border: '1px' }}>
                                    <Card.Text>
                                      <small className="text-muted">
                                        <span
                                          className="d-flex pb-3"
                                          style={{
                                            justifyContent: 'space-between',
                                          }}
                                        >
                                          <span>
                                            <img
                                              src={subreply.profileImageURL}
                                              alt={`${subreply.firstName} ${subreply.lastName}`}
                                              width="30"
                                              height="30"
                                              className="rounded-circle mr-2"
                                            />
                                            {subreply.firstName} {subreply.lastName} on{' '}
                                            {new Date(subreply.dateCreated).toDateString()}
                                          </span>
                                          {(subreply.userId === userInfo.userId ||
                                            userInfo.role === USER_TYPE.SUPER_ADMIN ||
                                            userInfo.role === USER_TYPE.ADMIN) && (
                                            <span>
                                              <Button
                                                variant="danger"
                                                type="submit"
                                                size="sm"
                                                onClick={(e) =>
                                                  handleDeleteSubReply(post.postId, index + 1, subIndex + 1)
                                                }
                                              >
                                                Delete
                                              </Button>
                                            </span>
                                          )}
                                        </span>
                                      </small>
                                    </Card.Text>
                                    {subreply.comment}
                                  </ListGroupItem>
                                ),
                            )}
                          </ListGroup>
                        )}
                        <div style={{ paddingTop: '15px' }}>
                          {
                            <form onSubmit={(event) => handleSubreplySubmit(event, postId, index)}>
                              <input
                                className="form-control"
                                id="Input-Subreply"
                                name="Input-Subreply"
                                placeholder="Write a subreply..."
                                onChange={(event) => handleSubReplyInputChange(event, index)}
                                value={subreplyComment[index].comment}
                              />
                              <div style={{ paddingTop: '5px', textAlign: 'end' }}>
                                {post.isArchived ||
                                userInfo.role === USER_TYPE.NORMAL_USER_NOT_VALID ||
                                userInfo.role === USER_TYPE.VISITOR_BANNED ? (
                                  <p>Replies are disabled for this post.</p>
                                ) : (
                                  <Button
                                    variant="outline-info"
                                    type="submit"
                                    size="sm"
                                    disabled={subreplyComment[index].comment === ''}
                                  >
                                    Submit
                                  </Button>
                                )}
                              </div>
                            </form>
                          }
                        </div>
                      </ListGroupItem>
                    ),
                )}
              </ListGroup>
            )}
            <div style={{ paddingTop: '15px' }}>
              <form onSubmit={(event) => handleReplySubmit(event, postId)}>
                <input
                  className="form-control"
                  id="Input-Reply"
                  name="Input-Reply"
                  placeholder="Write a reply..."
                  onChange={handleInputChange}
                  value={replyComment.comment}
                />
                <div style={{ paddingTop: '5px', textAlign: 'end' }}>
                  {post.isArchived ||
                  userInfo.role === USER_TYPE.NORMAL_USER_NOT_VALID ||
                  userInfo.role === USER_TYPE.VISITOR_BANNED ? (
                    <p>Replies are disabled for this post.</p>
                  ) : (
                    <Button variant="primary" type="submit" size="sm" disabled={replyComment.comment === ''}>
                      Submit
                    </Button>
                  )}
                </div>
              </form>
            </div>
          </Card.Body>
        </Card>
      )}
    </div>
  );
}

// Post.protoTypes = {};

export default PostDetail;
