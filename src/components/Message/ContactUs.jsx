// components/login.jsx
import React, { useState, useEffect } from 'react';
import { useLocation } from 'react-router';
import PropTypes from 'prop-types';
import { toast } from 'react-toastify';
import { dateParser } from '../../utils/utils';
import Table from 'react-bootstrap/Table';
import Button from 'react-bootstrap/Button';
import { USER_TYPE, USER_TYPE_INT, toUserType } from '../../constants/userType';
import { useSelector } from 'react-redux';
import Form from 'react-bootstrap/Form';
import FloatingLabel from 'react-bootstrap/FloatingLabel';
import LoadingModal from '../Common/LoadingModal.jsx';



const ContactUs = (props) => {
  const { isWaiting, errorMsg, createMessage } = props;
  const [subject, setSubject] = useState('');
  const [email, setEmail] = useState('');
  const [message, setMessage] = useState('');
  const [errors, setErrors] = useState({});

  const validateForm = () => {
    const errors = {};

    // Check if email is empty
    if (!email.trim()) {
      errors.email = 'Email is required';
    }else if (!isValidEmail(email)) {
      errors.email = 'Invalid email address.';
    }
    // Add more validation rules as needed (e.g., email format, password length, etc.)

    return errors;
  };

  const isValidEmail = (email) => {
    // You can add more sophisticated email validation here if needed.
    return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email);
  };

  const handleCreateMessage = async (e) => {
    const formErrors = validateForm();
    setErrors(formErrors);
    if (Object.keys(formErrors).length > 0) {
      return;
    }

    console.log(subject.length > 0 && email.length > 0 && message > 0);
    try {
      const postResponse = await createMessage(subject, email, message);

      if (postResponse.success) {
        toast.success("Message Sent");
        console.log('create message successfullly');
        window.location.reload();
      } else {
        toast.error(postResponse.message);
      }
    } catch (error) {
      toast.error(error);
    }
  };

  return (
    <>
      <LoadingModal isWaiting={isWaiting} errorMsg={errorMsg} />
      <div className="col-md-12">
        <div className="card card-container">
          <h1>Contact Us</h1>

          <Form>
            <Form.Group controlId="formBasicFirstName">
              <Form.Label>Subject</Form.Label>
              <Form.Control type="text" placeholder={subject} onChange={(e) => setSubject(e.target.value)} />
            </Form.Group>
            <Form.Group controlId="formBasicLastName">
              <Form.Label>Email Address</Form.Label>
              <Form.Control type="email" placeholder={email} onChange={(e) => setEmail(e.target.value)} />
            </Form.Group>
            {errors.email && <div className="error">{errors.email}</div>}
            <Form.Group controlId="formBasicLastName">
              <Form.Label>Message</Form.Label>
              <Form.Control
                as="textarea"
                placeholder="Leave a comment here"
                style={{ height: '100px', marginBottom: '10px' }}
                onChange={(e) => setMessage(e.target.value)}
              />
            </Form.Group>
          </Form>

          <Button
            onClick={handleCreateMessage}
            className="btn btn-primary btn-block"
            disabled={subject.length <= 0 || email.length <= 0 || message <= 0}
          >
            <span>Submit</span>
          </Button>
        </div>
      </div>
    </>
  );
};

ContactUs.propTypes = {
  isWaiting: PropTypes.bool.isRequired,
  errorMsg: PropTypes.string,
  createMessage: PropTypes.func.isRequired,
};

export default ContactUs;
