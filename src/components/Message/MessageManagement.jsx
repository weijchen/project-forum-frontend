// components/login.jsx
import React, { useState, useEffect } from 'react';
import { useLocation } from 'react-router';
import PropTypes from 'prop-types';
import { toast } from 'react-toastify';
import { dateParser } from '../../utils/utils';
import Table from 'react-bootstrap/Table';
import Button from 'react-bootstrap/Button';
import { USER_TYPE, USER_TYPE_INT, toUserType } from '../../constants/userType';
import { useSelector } from 'react-redux';
import LoadingModal from '../Common/LoadingModal.jsx';

const MessageManagement = (props) => {
  const { isWaiting, errorMsg, getAllMessages, updateMessageStatus } = props;
  const [messages, setMessages] = useState([]);
  const [messagesReversed, setMessagesReversed] = useState([]);
  const location = useLocation();

  const handleGetAllMessages = async (id) => {
    try {
      const response = await getAllMessages(id);
      if (response.success) {
        setMessages(response.data);
      } else {
        toast.error(response.message);
      }
    } catch (error) {
      toast.error(error);
    }
  };

  const handleUpdateMessageStatus = async (id) => {
    try {
      const response = await updateMessageStatus(id);
      if (response.success) {
        window.location.reload();
      } else {
        toast.error(response.message);
      }
    } catch (error) {
      toast.error(error);
    }
  };

  useEffect(() => {
    let newArray = [...messages];
    newArray.reverse();
    setMessagesReversed(newArray);
  }, [messages]);

  useEffect(() => {
    handleGetAllMessages();
  }, []);

  return (
    <>
      <LoadingModal isWaiting={isWaiting} errorMsg={errorMsg} />
      <div className="container-fluid m-2 p-4">
        <h1>Message Management</h1>
        <Table striped bordered hover>
          <thead>
            <tr>
              <th>#</th>
              <th>Date Created</th>
              <th>Subject</th>
              <th>Email</th>
              <th>Message</th>
              <th>Status</th>
              <th>Open/Close</th>
            </tr>
          </thead>
          <tbody>
            {messagesReversed.map((message, id) => (
              <>
                <tr>
                  <td>{id}</td>
                  <td>{dateParser(message.dateCreated)}</td>
                  <td>{message.subject}</td>
                  <td>{message.email}</td>
                  <td>{message.message}</td>
                  <td>{message.status.toUpperCase() === 'CLOSE' ? 'Close' : 'Open'}</td>
                  <td>
                    {message.status.toUpperCase() === 'OPEN' && (
                      <Button variant="primary" onClick={(e) => handleUpdateMessageStatus(message.messageId)}>
                        Close
                      </Button>
                    )}
                  </td>
                </tr>
              </>
            ))}
          </tbody>
        </Table>
      </div>
    </>
  );
};

MessageManagement.propTypes = {
  isWaiting: PropTypes.bool.isRequired,
  errorMsg: PropTypes.string,
  getAllMessages: PropTypes.func.isRequired,
  updateMessageStatus: PropTypes.func.isRequired,
};

export default MessageManagement;
