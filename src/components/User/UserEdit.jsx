import React from 'react';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import PropTypes from 'prop-types';

function UserEdit(props) {
  const {
    // isWaiting,
    // errorMsg,
    // show,
    onHide,
    firstname,
    lastname,
    email,
    password,
    setfirstname,
    setlastname,
    setemail,
    setpassword,
    setprofileimage,
    handleupdateuserprofile,
  } = props;

  return (
    <>
      <Modal {...props} size="lg" aria-labelledby="contained-modal-title-vcenter" centered>
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">Update Profile</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group controlId="formBasicFirstName">
              <Form.Label>First Name</Form.Label>
              <Form.Control type="text" placeholder={firstname} onChange={(e) => setfirstname(e.target.value)} />
            </Form.Group>
            <Form.Group controlId="formBasicLastName">
              <Form.Label>Last Name</Form.Label>
              <Form.Control type="text" placeholder={lastname} onChange={(e) => setlastname(e.target.value)} />
            </Form.Group>
            <Form.Group controlId="formBasicFirstName">
              <Form.Label>Email</Form.Label>
              <Form.Control type="email" placeholder={email} onChange={(e) => setemail(e.target.value)} />
            </Form.Group>
            <Form.Group controlId="formBasicFirstName">
              <Form.Label>Password</Form.Label>
              <Form.Control type="password" placeholder={password} onChange={(e) => setpassword(e.target.value)} />
            </Form.Group>
            <Form.Group controlId="formBasicFirstName">
              <Form.Label>Profile Image URL</Form.Label>
              <Form.Control type="file" onChange={(e) => setprofileimage(e.target.files[0])} />
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={handleupdateuserprofile}>Submit</Button>
          <Button onClick={onHide}>Close</Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}

UserEdit.propTypes = {
  // isWaiting: PropTypes.bool.isRequired,
  // errorMsg: PropTypes.string.isRequired,
  // show: PropTypes.bool.isRequired,
  onHide: PropTypes.func.isRequired,
  firstname: PropTypes.string.isRequired,
  lastname: PropTypes.string.isRequired,
  email: PropTypes.string.isRequired,
  password: PropTypes.string.isRequired,
  setfirstname: PropTypes.func.isRequired,
  setlastname: PropTypes.func.isRequired,
  setemail: PropTypes.func.isRequired,
  setpassword: PropTypes.func.isRequired,
  setprofileimage: PropTypes.func.isRequired,
  handleupdateuserprofile: PropTypes.func.isRequired,
};

export default UserEdit;
