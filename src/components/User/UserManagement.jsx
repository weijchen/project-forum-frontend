// components/login.jsx
import React, { useState, useEffect } from 'react';
import { useLocation } from 'react-router';
import PropTypes from 'prop-types';
import { toast } from 'react-toastify';
import { dateParser } from '../../utils/utils';
import Table from 'react-bootstrap/Table';
import Button from 'react-bootstrap/Button';
import { USER_TYPE, USER_TYPE_INT, toUserType } from '../../constants/userType';
import { useSelector } from 'react-redux';
import LoadingModal from '../Common/LoadingModal.jsx';

const UserManagement = (props) => {
  const { isWaiting, errorMsg, getAllUsers, promoteUser, banUnbanUser } = props;
  const [users, setUsers] = useState([]);
  const location = useLocation();
  const userInfo = useSelector((state) => state.auth.userInfo);

  const handleGetAllUsers = async (id) => {
    try {
      const userResponse = await getAllUsers(id);
      if (userResponse.success) {
        setUsers(userResponse.data);
      } else {
        toast.error(userResponse.message);
      }
    } catch (error) {
      toast.error(error);
    }
  };

  const handlePromoteUser = async (id) => {
    try {
      const userResponse = await promoteUser(id);
      if (userResponse.success) {
        window.location.reload();
      } else {
        toast.error(userResponse.message);
      }
    } catch (error) {
      toast.error(error);
    }
  };

  const handleBanUnbanUser = async (id) => {
    try {
      const userResponse = await banUnbanUser(id);
      if (userResponse.success) {
        window.location.reload();
      } else {
        toast.error(userResponse.message);
      }
    } catch (error) {
      toast.error(error);
    }
  };

  useEffect(() => {
    handleGetAllUsers();
  }, [location]);

  return (
    <>
      <LoadingModal isWaiting={isWaiting} errorMsg={errorMsg} />
      <div className="container-fluid m-2 p-4">
        <h1>User Management</h1>
        <Table striped bordered hover>
          <thead>
            <tr>
              <th>#</th>
              <th>User ID</th>
              <th>Full Name</th>
              <th>Email</th>
              <th>Date Joined</th>
              <th>Type</th>
              <th>Ban/Unban</th>
              {userInfo.role === USER_TYPE.SUPER_ADMIN && <th>Promote</th>}
            </tr>
          </thead>
          <tbody>
            {users.map((user, id) => (
              <>
                <tr>
                  <td>{id}</td>
                  <td>{user.id}</td>
                  <td>{user.firstName + ' ' + user.lastName}</td>
                  <td>{user.email}</td>
                  <td>{dateParser(user.dateJoined)}</td>
                  <td>{toUserType(user.type)}</td>
                  <td>
                    {!user.active ? (
                      <Button variant="secondary" onClick={(e) => handleBanUnbanUser(user.id)}>
                        Unban
                      </Button>
                    ) : (
                      <Button
                        variant="primary"
                        onClick={(e) => handleBanUnbanUser(user.id)}
                        disabled={user.type === USER_TYPE_INT.SUPER_ADMIN || user.type === USER_TYPE_INT.ADMIN}
                      >
                        Ban
                      </Button>
                    )}
                  </td>
                  {userInfo.role === USER_TYPE.SUPER_ADMIN && (
                    <td>
                      {user.type === USER_TYPE_INT.NORMAL_USER && user.active && (
                        <Button variant="primary" onClick={(e) => handlePromoteUser(user.id)}>
                          Promote
                        </Button>
                      )}
                    </td>
                  )}
                </tr>
              </>
            ))}
          </tbody>
        </Table>
      </div>
    </>
  );
};

UserManagement.propTypes = {
  isWaiting: PropTypes.bool.isRequired,
  errorMsg: PropTypes.string,
  getAllUsers: PropTypes.func.isRequired,
  promoteUser: PropTypes.func.isRequired,
  banUnbanUser: PropTypes.func.isRequired,
};

export default UserManagement;
