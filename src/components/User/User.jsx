import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import { useLocation } from 'react-router';
import { toast } from 'react-toastify';
import '../../css/user.css';
import { PostStatusStr, dateParser } from '../../utils/utils';
import UserEdit from '../../containers/User/UserEdit';
import Button from 'react-bootstrap/Button';
import Tab from 'react-bootstrap/Tab';
import Tabs from 'react-bootstrap/Tabs';
import LoadingModal from '../Common/LoadingModal.jsx';
import Badge from 'react-bootstrap/Badge';
import agent from '../../lib/agent.js';
import { Col, Form, Row } from 'react-bootstrap';
import { USER_TYPE, toUserType } from '../../constants/userType';

function User(props) {
  const {
    isWaiting,
    errorMsg,
    getUserProfileById,
    getUnpublishedPosts,
    getThreePostWithMostReviews,
    getViewedPost,
    updateUserProfile,
    getUserPublishedPosts,
    getUserBannedPosts,
    getUserHiddenPosts,
    getUserDeletedPosts,
    updateHistory,
    getViewedOnDate,
    getViewedContainWord,
    sendVerificationEmail,
  } = props;
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [role, setRole] = useState('');
  const [profileImageURL, setProfileImageURL] = useState('');
  const [dateRegistered, setDateRegistered] = useState('');
  const [unpublishedPosts, setUnpublishedPosts] = useState([]);
  const [topPostsWithReply, setTopPostsWithReply] = useState([]);
  const [viewedPost, setViewedPost] = useState([]);
  const [viewedPostOnDate, setViewedPostOnDate] = useState([]);
  const [viewedPostContainWord, setViewedPostContainWord] = useState([]);
  const [userPublishedPost, setUserPublishedPost] = useState([]);
  const [userBannedPost, setUserBannedPost] = useState([]);
  const [userHiddenPost, setUserHiddenPost] = useState([]);
  const [userDeletedPost, setUserDeletedPost] = useState([]);
  const [modalShow, setModalShow] = useState(false);
  const [newFirstName, setNewFirstName] = useState('');
  const [newLastName, setNewLastName] = useState('');
  const [newEmail, setNewEmail] = useState('');
  const [newPassword, setNewPassword] = useState('');
  const [newProfileImage, setNewProfileImage] = useState(new FormData());
  const [viewedDate, setViewedDate] = useState(new Date().toISOString());
  const [containWord, setContainWord] = useState('');
  const [sendEmailDesc, setSendEmailDesc] = useState('Send Verification Email');
  const [updateProfileDesc, setUpdateProfileDesc] = useState('Update Profile');
  const [infoChanged, setInfoChanged] = useState(false);
  const location = useLocation();
  const userInfo = useSelector((state) => state.auth.userInfo);

  const handleGetUserProfile = async (id) => {
    try {
      const userResponse = await getUserProfileById(id);
      if (userResponse.success) {
        setRole(toUserType(userResponse.data.type));
        setFirstName(userResponse.data.firstName);
        setLastName(userResponse.data.lastName);
        setEmail(userResponse.data.email);
        setProfileImageURL(userResponse.data.profileImageURL);
        setDateRegistered(userResponse.data.dateJoined);
        setNewFirstName(userResponse.data.firstName);
        setNewLastName(userResponse.data.lastName);
        setNewEmail(userResponse.data.email);
        setInfoChanged(false);
      } else {
        toast.error(userResponse.message);
      }
    } catch (error) {
      toast.error(error);
    }
  };

  const handleGetUnpublishedPosts = async () => {
    try {
      const postResponse = await getUnpublishedPosts();
      if (postResponse.success) {
        setUnpublishedPosts(postResponse.data);
      } else {
        toast.error(postResponse.message);
      }
    } catch (error) {
      toast.error(error);
    }
  };

  const handleDelete = async (postId) => {
    try {
      const postResponse = await agent.Post.deletePost(postId);

      if (postResponse.statusText === 'OK') {
        console.log(postResponse);
      } else {
        console.log(postResponse.message);
        toast.error(postResponse.message);
      }
      window.location.reload();
    } catch (error) {
      toast.error(error);
    }
  };

  const handleGetThreePostWithMostReviews = async () => {
    try {
      const postResponse = await getThreePostWithMostReviews();
      if (postResponse.success) {
        setTopPostsWithReply(postResponse.data);
      } else {
        toast.error(postResponse.message);
      }
    } catch (error) {
      toast.error(error);
    }
  };

  const handleGetViewedPost = async () => {
    try {
      const postResponse = await getViewedPost();
      if (postResponse.success) {
        setViewedPost(postResponse.data.body.historyWithPostDetailList);
      } else {
        toast.error(postResponse.message);
      }
    } catch (error) {
      toast.error(error);
    }
  };

  const handleUpdateUserProfile = async () => {
    setModalShow(false);

    try {
      const postResponse = await updateUserProfile(
        userInfo.userId,
        newEmail,
        newFirstName,
        newLastName,
        newPassword,
        newProfileImage,
      );

      if (postResponse.success) {
        setInfoChanged(true);
        toast.success('Profile update success');
      } else {
        toast.error(postResponse.message);
      }
    } catch (error) {
      toast.error(error);
    }
  };

  const handleGetUserPublishedPost = async () => {
    try {
      const postResponse = await getUserPublishedPosts();
      if (postResponse.success) {
        setUserPublishedPost(postResponse.data);
      } else {
        toast.error(postResponse.message);
      }
    } catch (error) {
      toast.error(error);
    }
  };

  const handleGetUserBannedPost = async () => {
    try {
      const postResponse = await getUserBannedPosts();
      if (postResponse.success) {
        setUserBannedPost(postResponse.data);
      } else {
        toast.error(postResponse.message);
      }
    } catch (error) {
      toast.error(error);
    }
  };

  const handleGetUserHiddenPost = async () => {
    try {
      const postResponse = await getUserHiddenPosts();
      if (postResponse.success) {
        setUserHiddenPost(postResponse.data);
      } else {
        toast.error(postResponse.message);
      }
    } catch (error) {
      toast.error(error);
    }
  };

  const handleGetUserDeletedPost = async () => {
    try {
      const postResponse = await getUserDeletedPosts();
      if (postResponse.success) {
        setUserDeletedPost(postResponse.data);
      } else {
        toast.error(postResponse.message);
      }
    } catch (error) {
      toast.error(error);
    }
  };

  const handleUpdateHistory = async (postId, isPublished) => {
    try {
      const postResponse = await updateHistory(postId, isPublished);
      if (postResponse.success) {
        console.log('Successfully update history');
      } else {
        if (isPublished) toast.error(postResponse.message);
      }
    } catch (error) {
      toast.error(error);
    }
  };

  const handleGetViewedPostOnDate = async () => {
    try {
      const postResponse = await getViewedOnDate(viewedDate);
      if (postResponse.success) {
        console.log(postResponse);
        setViewedPostOnDate(postResponse.data.body.historyWithPostDetailList);
      } else {
        toast.error(postResponse.message);
      }
    } catch (error) {
      toast.error(error);
    }
  };

  const handleGetViewedPostContainWord = async () => {
    try {
      const postResponse = await getViewedContainWord(containWord);
      if (postResponse.success) {
        console.log(postResponse);
        setViewedPostContainWord(postResponse.data.body.historyWithPostDetailList);
      } else {
        toast.error(postResponse.message);
      }
    } catch (error) {
      toast.error(error);
    }
  };

  const handleSendVerificationEmail = async (email, uid) => {
    try {
      const response = await sendVerificationEmail(email, uid);
      if (response.success) {
        toast.success('Verification email sent');
        setInfoChanged(true);
      } else {
        toast.error(response.message);
      }
    } catch (error) {
      toast.error(error);
    }
  };

  useEffect(() => {
    handleGetUserProfile(userInfo.userId);
    handleGetUnpublishedPosts();
    handleGetThreePostWithMostReviews();
    handleGetViewedPost();
    handleGetUserPublishedPost();
    handleGetUserBannedPost();
    handleGetUserHiddenPost();
    handleGetUserDeletedPost();
  }, [location]);

  useEffect(() => {
    handleGetUserProfile(userInfo.userId);
  }, [infoChanged]);

  return (
    <>
      <LoadingModal isWaiting={isWaiting} errorMsg={errorMsg} />
      <UserEdit
        show={modalShow}
        onHide={() => setModalShow(false)}
        firstName={newFirstName}
        lastName={newLastName}
        email={newEmail}
        password={newPassword}
        profileImage={newProfileImage}
        setFirstName={setNewFirstName}
        setLastName={setNewLastName}
        setEmail={setNewEmail}
        setPassword={setNewPassword}
        setProfileImage={setNewProfileImage}
        handleUpdateUserProfile={handleUpdateUserProfile}
      />
      <div className="container-fluid">
        <div className="d-flex user_board">
          <div className="col-6" id="user_board_left">
            <Tabs defaultActiveKey="profile" id="fill-tab-example" className="mb-3" fill>
              <Tab eventKey="profile" title="User Profile">
                <div className="card">
                  <div className="card-body d-flex" style={{ justifyContent: 'space-evenly' }}>
                    <div>
                      <img
                        src={profileImageURL}
                        alt={`${firstName}-${lastName}`}
                        width="150"
                        height="150"
                        className="rounded-circle mr-2"
                      />
                    </div>
                    <div>
                      <h5 className="card-title">
                        Hi! {firstName} {lastName}
                      </h5>

                      <p className="card-text">Email: {email}</p>
                      <p className="card-text">Account Created: {dateParser(dateRegistered)}</p>
                      {role === USER_TYPE.NORMAL_USER_NOT_VALID ? (
                        <Button variant="primary" onClick={() => handleSendVerificationEmail(email, userInfo.userId)}>
                          {sendEmailDesc}
                        </Button>
                      ) : (
                        <Button variant="primary" onClick={() => setModalShow(true)}>
                          {updateProfileDesc}
                        </Button>
                      )}
                    </div>
                  </div>
                </div>
              </Tab>
              <Tab eventKey="most-replies" title="Post with Most replies">
                {topPostsWithReply.length !== 0 &&
                  topPostsWithReply.map((post) => (
                    <>
                      <div className="card">
                        <div className="card-body">
                          <div className="d-flex" style={{ justifyContent: 'space-between' }}>
                            <div>
                              <h5 className="card-title">
                                {post.title.length >= 30 ? post.title.substring(0, 30) + '...' : post.title}
                              </h5>
                            </div>
                            <div>
                              {post.status === PostStatusStr.Published && (
                                <Badge bg="primary" style={{ color: 'white' }}>
                                  {post.status}
                                </Badge>
                              )}
                              {post.status === PostStatusStr.Unpublished && (
                                <Badge bg="info" style={{ color: 'white' }}>
                                  {post.status}
                                </Badge>
                              )}
                              {post.status === PostStatusStr.Hidden && (
                                <Badge bg="secondary" style={{ color: 'white' }}>
                                  {post.status}
                                </Badge>
                              )}
                              {post.status === PostStatusStr.Banned && (
                                <Badge bg="warning" style={{ color: 'white' }}>
                                  {post.status}
                                </Badge>
                              )}
                              {post.status === PostStatusStr.Deleted && (
                                <Badge bg="danger" style={{ color: 'white' }}>
                                  {post.status}
                                </Badge>
                              )}
                            </div>
                          </div>
                          <p className="card-text">{post.content}</p>
                          <p className="card-text">Created: {dateParser(post.dateCreated)}</p>
                          <Button
                            variant="primary"
                            onClick={() => handleUpdateHistory(post.postId, post.status === PostStatusStr.Published)}
                          >
                            Open Post
                          </Button>{' '}
                          <Button variant="warning" onClick={() => handleDelete(post.postId)}>
                            Delete Post
                          </Button>
                        </div>
                      </div>
                    </>
                  ))}
              </Tab>
              <Tab eventKey="published-tab" title="All Published posts">
                {userPublishedPost.length !== 0 &&
                  userPublishedPost.map((post) => (
                    <>
                      <div className="card">
                        <div className="card-body">
                          <div className="d-flex" style={{ justifyContent: 'space-between' }}>
                            <div>
                              <h5 className="card-title">
                                {post.title.length >= 30 ? post.title.substring(0, 30) + '...' : post.title}
                              </h5>
                            </div>
                            <div>
                              {post.status === PostStatusStr.Published && (
                                <Badge bg="primary" style={{ color: 'white' }}>
                                  {post.status}
                                </Badge>
                              )}
                              {post.status === PostStatusStr.Unpublished && (
                                <Badge bg="info" style={{ color: 'white' }}>
                                  {post.status}
                                </Badge>
                              )}
                              {post.status === PostStatusStr.Hidden && (
                                <Badge bg="secondary" style={{ color: 'white' }}>
                                  {post.status}
                                </Badge>
                              )}
                              {post.status === PostStatusStr.Banned && (
                                <Badge bg="warning" style={{ color: 'white' }}>
                                  {post.status}
                                </Badge>
                              )}
                              {post.status === PostStatusStr.Deleted && (
                                <Badge bg="danger" style={{ color: 'white' }}>
                                  {post.status}
                                </Badge>
                              )}
                            </div>
                          </div>
                          <p className="card-text">{post.content}</p>
                          <p className="card-text">Created: {dateParser(post.dateCreated)}</p>
                          <Button
                            variant="primary"
                            onClick={() => handleUpdateHistory(post.postId, post.status === PostStatusStr.Published)}
                          >
                            Open Post
                          </Button>
                        </div>
                      </div>
                    </>
                  ))}
              </Tab>
              <Tab eventKey="unpublished-tab" title="All Unpublished posts">
                {unpublishedPosts.length !== 0 &&
                  unpublishedPosts.map((post) => (
                    <>
                      <div className="card">
                        <div className="card-body">
                          <div className="d-flex" style={{ justifyContent: 'space-between' }}>
                            <div>
                              <h5 className="card-title">
                                {post.title.length >= 30 ? post.title.substring(0, 30) + '...' : post.title}
                              </h5>
                            </div>
                            <div>
                              {post.status === PostStatusStr.Published && (
                                <Badge bg="primary" style={{ color: 'white' }}>
                                  {post.status}
                                </Badge>
                              )}
                              {post.status === PostStatusStr.Unpublished && (
                                <Badge bg="info" style={{ color: 'white' }}>
                                  {post.status}
                                </Badge>
                              )}
                              {post.status === PostStatusStr.Hidden && (
                                <Badge bg="secondary" style={{ color: 'white' }}>
                                  {post.status}
                                </Badge>
                              )}
                              {post.status === PostStatusStr.Banned && (
                                <Badge bg="warning" style={{ color: 'white' }}>
                                  {post.status}
                                </Badge>
                              )}
                              {post.status === PostStatusStr.Deleted && (
                                <Badge bg="danger" style={{ color: 'white' }}>
                                  {post.status}
                                </Badge>
                              )}
                            </div>
                          </div>
                          <p className="card-text">{post.content}</p>
                          <p className="card-text">Created: {dateParser(post.dateCreated)}</p>
                          <Button
                            variant="primary"
                            onClick={() => handleUpdateHistory(post.postId, post.status === PostStatusStr.Published)}
                          >
                            Open Post
                          </Button>
                        </div>
                      </div>
                    </>
                  ))}
              </Tab>
              <Tab eventKey="banned-tab" title="All Banned posts">
                {userBannedPost.length !== 0 &&
                  userBannedPost.map((post) => (
                    <>
                      <div className="card">
                        <div className="card-body">
                          <div className="d-flex" style={{ justifyContent: 'space-between' }}>
                            <div>
                              <h5 className="card-title">
                                {post.title.length >= 30 ? post.title.substring(0, 30) + '...' : post.title}
                              </h5>
                            </div>
                            <div>
                              {post.status === PostStatusStr.Published && (
                                <Badge bg="primary" style={{ color: 'white' }}>
                                  {post.status}
                                </Badge>
                              )}
                              {post.status === PostStatusStr.Unpublished && (
                                <Badge bg="info" style={{ color: 'white' }}>
                                  {post.status}
                                </Badge>
                              )}
                              {post.status === PostStatusStr.Hidden && (
                                <Badge bg="secondary" style={{ color: 'white' }}>
                                  {post.status}
                                </Badge>
                              )}
                              {post.status === PostStatusStr.Banned && (
                                <Badge bg="warning" style={{ color: 'white' }}>
                                  {post.status}
                                </Badge>
                              )}
                              {post.status === PostStatusStr.Deleted && (
                                <Badge bg="danger" style={{ color: 'white' }}>
                                  {post.status}
                                </Badge>
                              )}
                            </div>
                          </div>
                          <p className="card-text">{post.content}</p>
                          <p className="card-text">Created: {dateParser(post.dateCreated)}</p>
                          <Button
                            variant="primary"
                            onClick={() => handleUpdateHistory(post.postId, post.status === PostStatusStr.Published)}
                          >
                            Open Post
                          </Button>
                        </div>
                      </div>
                    </>
                  ))}
              </Tab>
              <Tab eventKey="hidden-tab" title="All Hidden posts">
                {userHiddenPost.length !== 0 &&
                  userHiddenPost.map((post) => (
                    <>
                      <div className="card">
                        <div className="card-body">
                          <div className="d-flex" style={{ justifyContent: 'space-between' }}>
                            <div>
                              <h5 className="card-title">
                                {post.title.length >= 30 ? post.title.substring(0, 30) + '...' : post.title}
                              </h5>
                            </div>
                            <div>
                              {post.status === PostStatusStr.Published && (
                                <Badge bg="primary" style={{ color: 'white' }}>
                                  {post.status}
                                </Badge>
                              )}
                              {post.status === PostStatusStr.Unpublished && (
                                <Badge bg="info" style={{ color: 'white' }}>
                                  {post.status}
                                </Badge>
                              )}
                              {post.status === PostStatusStr.Hidden && (
                                <Badge bg="secondary" style={{ color: 'white' }}>
                                  {post.status}
                                </Badge>
                              )}
                              {post.status === PostStatusStr.Banned && (
                                <Badge bg="warning" style={{ color: 'white' }}>
                                  {post.status}
                                </Badge>
                              )}
                              {post.status === PostStatusStr.Deleted && (
                                <Badge bg="danger" style={{ color: 'white' }}>
                                  {post.status}
                                </Badge>
                              )}
                            </div>
                          </div>
                          <p className="card-text">{post.content}</p>
                          <p className="card-text">Created: {dateParser(post.dateCreated)}</p>
                          <Button
                            variant="primary"
                            onClick={() => handleUpdateHistory(post.postId, post.status === PostStatusStr.Published)}
                          >
                            Open Post
                          </Button>
                        </div>
                      </div>
                    </>
                  ))}
              </Tab>
              <Tab eventKey="deleted-tab" title="All Deleted posts">
                {userDeletedPost.length !== 0 &&
                  userDeletedPost.map((post) => (
                    <>
                      <div className="card">
                        <div className="card-body">
                          <div className="d-flex" style={{ justifyContent: 'space-between' }}>
                            <div>
                              <h5 className="card-title">
                                {post.title.length >= 30 ? post.title.substring(0, 30) + '...' : post.title}
                              </h5>
                            </div>
                            <div>
                              {post.status === PostStatusStr.Published && (
                                <Badge bg="primary" style={{ color: 'white' }}>
                                  {post.status}
                                </Badge>
                              )}
                              {post.status === PostStatusStr.Unpublished && (
                                <Badge bg="info" style={{ color: 'white' }}>
                                  {post.status}
                                </Badge>
                              )}
                              {post.status === PostStatusStr.Hidden && (
                                <Badge bg="secondary" style={{ color: 'white' }}>
                                  {post.status}
                                </Badge>
                              )}
                              {post.status === PostStatusStr.Banned && (
                                <Badge bg="warning" style={{ color: 'white' }}>
                                  {post.status}
                                </Badge>
                              )}
                              {post.status === PostStatusStr.Deleted && (
                                <Badge bg="danger" style={{ color: 'white' }}>
                                  {post.status}
                                </Badge>
                              )}
                            </div>
                          </div>
                          <p className="card-text">{post.content}</p>
                          <p className="card-text">Created: {dateParser(post.dateCreated)}</p>
                          <Button
                            variant="primary"
                            onClick={() => handleUpdateHistory(post.postId, post.status === PostStatusStr.Published)}
                          >
                            Open Post
                          </Button>
                        </div>
                      </div>
                    </>
                  ))}
              </Tab>
            </Tabs>
          </div>

          <div className="col-6" id="user_board_right">
            <Tabs defaultActiveKey="recent" id="fill-tab-example" className="mb-3" fill>
              <Tab eventKey="recent" title="Recently View History">
                <h2 id="user_board_right_title">Recently view history</h2>
                {viewedPost.length !== 0 &&
                  viewedPost.map((post, _) => (
                    <>
                      <div className="card" key={_}>
                        <div className="card-body">
                          <div className="d-flex" style={{ justifyContent: 'space-between' }}>
                            <div>
                              <h5 className="card-title">
                                {post.post.title.length >= 30
                                  ? post.post.title.substring(0, 30) + '...'
                                  : post.post.title}
                              </h5>
                            </div>
                            <div>
                              {post.post.status === PostStatusStr.Published && (
                                <Badge bg="primary" style={{ color: 'white' }}>
                                  {post.post.status}
                                </Badge>
                              )}
                              {post.post.status === PostStatusStr.Unpublished && (
                                <Badge bg="info" style={{ color: 'white' }}>
                                  {post.post.status}
                                </Badge>
                              )}
                              {post.post.status === PostStatusStr.Hidden && (
                                <Badge bg="secondary" style={{ color: 'white' }}>
                                  {post.post.status}
                                </Badge>
                              )}
                              {post.post.status === PostStatusStr.Banned && (
                                <Badge bg="warning" style={{ color: 'white' }}>
                                  {post.post.status}
                                </Badge>
                              )}
                              {post.post.status === PostStatusStr.Deleted && (
                                <Badge bg="danger" style={{ color: 'white' }}>
                                  {post.post.status}
                                </Badge>
                              )}
                            </div>
                          </div>
                          <p className="card-text">{post.post.content}</p>
                          <p className="card-text">Created: {dateParser(post.post.dateCreated)}</p>
                          <Button
                            variant="primary"
                            onClick={() =>
                              handleUpdateHistory(post.postId, post.post.status === PostStatusStr.Published)
                            }
                          >
                            Open Post
                          </Button>
                        </div>
                      </div>
                    </>
                  ))}
              </Tab>
              <Tab eventKey="onDate" title="History On Date">
                <h2 id="user_board_right_title">History On Date</h2>
                <Form className="d-flex">
                  <Form.Control
                    type="date"
                    placeholder="Date"
                    className="me-2 rounded-pill"
                    value={viewedDate || ''}
                    aria-label="Search"
                    onChange={(e) => setViewedDate(e.target.value)}
                  />
                  <Button
                    className="rounded-pill"
                    variant="outline-primary"
                    onClick={handleGetViewedPostOnDate}
                    style={{ marginLeft: '5px' }}
                  >
                    Apply
                  </Button>
                </Form>
                {viewedPostOnDate.length !== 0 &&
                  viewedPostOnDate.map((post, _) => (
                    <>
                      <div className="card" key={_}>
                        <div className="card-body">
                          <div className="d-flex" style={{ justifyContent: 'space-between' }}>
                            <div>
                              <h5 className="card-title">
                                {post.post.title.length >= 30
                                  ? post.post.title.substring(0, 30) + '...'
                                  : post.post.title}
                              </h5>
                            </div>
                            <div>
                              {post.post.status === PostStatusStr.Published && (
                                <Badge bg="primary" style={{ color: 'white' }}>
                                  {post.post.status}
                                </Badge>
                              )}
                              {post.post.status === PostStatusStr.Unpublished && (
                                <Badge bg="info" style={{ color: 'white' }}>
                                  {post.post.status}
                                </Badge>
                              )}
                              {post.post.status === PostStatusStr.Hidden && (
                                <Badge bg="secondary" style={{ color: 'white' }}>
                                  {post.post.status}
                                </Badge>
                              )}
                              {post.post.status === PostStatusStr.Banned && (
                                <Badge bg="warning" style={{ color: 'white' }}>
                                  {post.post.status}
                                </Badge>
                              )}
                              {post.post.status === PostStatusStr.Deleted && (
                                <Badge bg="danger" style={{ color: 'white' }}>
                                  {post.post.status}
                                </Badge>
                              )}
                            </div>
                          </div>
                          <p className="card-text">{post.post.content}</p>
                          <p className="card-text">Created: {dateParser(post.post.dateCreated)}</p>
                          <Button
                            variant="primary"
                            onClick={() =>
                              handleUpdateHistory(post.postId, post.post.status === PostStatusStr.Published)
                            }
                          >
                            Open Post
                          </Button>
                        </div>
                      </div>
                    </>
                  ))}
              </Tab>
              <Tab eventKey="containWord" title="History Contain word">
                <h2 id="user_board_right_title">History Contain word</h2>
                <Form className="d-flex">
                  <Form.Control
                    type="search"
                    placeholder="Word"
                    className="me-2 rounded-pill"
                    value={containWord || ''}
                    aria-label="Search"
                    onChange={(e) => setContainWord(e.target.value)}
                  />
                  <Button
                    className="rounded-pill"
                    variant="outline-primary"
                    onClick={handleGetViewedPostContainWord}
                    style={{ marginLeft: '5px' }}
                  >
                    Apply
                  </Button>
                </Form>
                {viewedPostContainWord.length !== 0 &&
                  viewedPostContainWord.map((post, _) => (
                    <>
                      <div className="card" key={_}>
                        <div className="card-body">
                          <div className="d-flex" style={{ justifyContent: 'space-between' }}>
                            <div>
                              <h5 className="card-title">
                                {post.post.title.length >= 30
                                  ? post.post.title.substring(0, 30) + '...'
                                  : post.post.title}
                              </h5>
                            </div>
                            <div>
                              {post.post.status === PostStatusStr.Published && (
                                <Badge bg="primary" style={{ color: 'white' }}>
                                  {post.post.status}
                                </Badge>
                              )}
                              {post.post.status === PostStatusStr.Unpublished && (
                                <Badge bg="info" style={{ color: 'white' }}>
                                  {post.post.status}
                                </Badge>
                              )}
                              {post.post.status === PostStatusStr.Hidden && (
                                <Badge bg="secondary" style={{ color: 'white' }}>
                                  {post.post.status}
                                </Badge>
                              )}
                              {post.post.status === PostStatusStr.Banned && (
                                <Badge bg="warning" style={{ color: 'white' }}>
                                  {post.post.status}
                                </Badge>
                              )}
                              {post.post.status === PostStatusStr.Deleted && (
                                <Badge bg="danger" style={{ color: 'white' }}>
                                  {post.post.status}
                                </Badge>
                              )}
                            </div>
                          </div>
                          <p className="card-text">{post.post.content}</p>
                          <p className="card-text">Created: {dateParser(post.post.dateCreated)}</p>
                          <Button
                            variant="primary"
                            onClick={() =>
                              handleUpdateHistory(post.postId, post.post.status === PostStatusStr.Published)
                            }
                          >
                            Open Post
                          </Button>
                        </div>
                      </div>
                    </>
                  ))}
              </Tab>
            </Tabs>
          </div>
        </div>
      </div>
    </>
  );
}

User.propTypes = {
  isWaiting: PropTypes.bool.isRequired,
  errorMsg: PropTypes.string.isRequired,
  getUserProfileById: PropTypes.func.isRequired,
  getUnpublishedPosts: PropTypes.func.isRequired,
  getThreePostWithMostReviews: PropTypes.func.isRequired,
  getViewedPost: PropTypes.func.isRequired,
  updateUserProfile: PropTypes.func.isRequired,
  getUserPublishedPosts: PropTypes.func.isRequired,
  getUserBannedPosts: PropTypes.func.isRequired,
  getUserHiddenPosts: PropTypes.func.isRequired,
  getUserDeletedPosts: PropTypes.func.isRequired,
  updateHistory: PropTypes.func.isRequired,
  getViewedOnDate: PropTypes.func.isRequired,
  getViewedContainWord: PropTypes.func.isRequired,
  sendVerificationEmail: PropTypes.func.isRequired,
};

export default User;
