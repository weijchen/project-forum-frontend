import '../../css/style.css';
import React from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import Spinner from 'react-bootstrap/Spinner';

function LoadingModal(props) {
  const { isWaiting, errorMsg } = props;

  return (
    <>
      <div>
        <div className={'loading_modal_background ' + (isWaiting === false ? 'loading_modal_disable' : '')}>
          <Spinner
            animation="grow"
            className={'loading_modal ' + (isWaiting === false ? 'loading_modal_disable' : '')}
          />
        </div>
      </div>
    </>
  );
}

LoadingModal.protoTypes = {};

export default LoadingModal;
