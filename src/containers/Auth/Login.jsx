import React, { useEffect, useState } from 'react';
import Layout from '../../components/Auth/Login';
import agent from '../../lib/agent';
import { setTokenValid } from '../../redux/actions/authActions.js';
import { useSelector } from 'react-redux';

function Login(props) {
  // const { test } = props;
  const [loadRole, setLoadRole] = useState(true);
  const [isWaiting, setIsWaiting] = useState(false);
  const [errorMsg, setErrorMsg] = useState('');
  const userInfo = useSelector((state) => state.auth.userInfo);
  // const { setTokenValid } = props;

  const login = async (email, password) => {
    setErrorMsg('');
    setIsWaiting(true);
    try {
      const body = { email: email, password: password };
      const result = await agent.Auth.login(body);
      setIsWaiting(false);
      setTokenValid(result.data.success);
      return result.data;
    } catch (error) {
      setIsWaiting(false);
      setErrorMsg(error);
      return error;
    }
  };

  const getUserProfileById = async (userId) => {
    setErrorMsg('');
    setIsWaiting(true);
    const params = { userId: userId };
    try {
      const result = await agent.User.getUserProfile(params);
      setIsWaiting(false);
      return result.data;
    } catch (error) {
      setIsWaiting(false);
      setErrorMsg(error);
      return error;
    }
  };

  useEffect(() => {
    if (userInfo.role != null) {
      window.location.href = '/home';
    } else {
      setLoadRole(false);
    }
  }, []);

  return loadRole ? (
    <></>
  ) : (
    <Layout isWaiting={isWaiting} errorMsg={errorMsg} login={login} getUserProfileById={getUserProfileById} />
  );
}

// Login.propTypes = {};

export default Login;
