import React, { useState, useEffect } from 'react';
import Layout from '../../components/Auth/Register';
import agent from '../../lib/agent';
import { useSelector } from 'react-redux';

function Register(props) {
  // const { test } = props;
  const [loadRole, setLoadRole] = useState(true);
  const [isWaiting, setIsWaiting] = useState(false);
  const [errorMsg, setErrorMsg] = useState('');
  const userInfo = useSelector((state) => state.auth.userInfo);
  // const { setTokenValid } = props;

  const register = async (firstName, lastName, email, password) => {
    setErrorMsg('');
    setIsWaiting(true);
    try {
      const body = { firstName: firstName, lastName: lastName, email: email, password: password };
      const result = await agent.Auth.register(body);
      setIsWaiting(false);
      return result.data;
    } catch (error) {
      setIsWaiting(false);
      setErrorMsg(error);
      return error;
    }
  };

  useEffect(() => {
    if (userInfo.role != null) {
      window.location.href = '/home';
    } else {
      setLoadRole(false);
    }
  }, []);

  return loadRole ? <></> : <Layout isWaiting={isWaiting} errorMsg={errorMsg} register={register} />;
}

export default Register;
