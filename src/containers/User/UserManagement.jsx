import React, { useState } from 'react';
import agent from '../../lib/agent';
import Layout from '../../components/User/UserManagement';

function UserManagement(props) {
  const [isWaiting, setIsWaiting] = useState(false);
  const [errorMsg, setErrorMsg] = useState(null);

  const getAllUsers = async () => {
    setErrorMsg(null);
    setIsWaiting(true);
    try {
      const result = await agent.User.getAllUsers();
      setIsWaiting(false);
      return result.data;
    } catch (error) {
      setIsWaiting(false);
      setErrorMsg(error);
      return error;
    }
  };

  const promoteUser = async (id) => {
    setErrorMsg(null);
    setIsWaiting(true);
    try {
      const result = await agent.User.promoteUser(id);
      setIsWaiting(false);
      return result.data;
    } catch (error) {
      setIsWaiting(false);
      setErrorMsg(error);
      return error;
    }
  };

  const banUnbanUser = async (id, type) => {
    setErrorMsg(null);
    setIsWaiting(true);
    try {
      const result = await agent.User.banUnbanUser(id);
      setIsWaiting(false);
      return result.data;
    } catch (error) {
      setIsWaiting(false);
      setErrorMsg(error);
      return error;
    }
  };

  return (
    <Layout
      isWaiting={isWaiting}
      errorMsg={errorMsg}
      getAllUsers={getAllUsers}
      promoteUser={promoteUser}
      banUnbanUser={banUnbanUser}
    />
  );
}

UserManagement.protoTypes = {};

export default UserManagement;
