import React, { useState } from 'react';
import agent from '../../lib/agent';
import Layout from '../../components/User/User';
import { useNavigate } from 'react-router-dom';

function User(props) {
  const [isWaiting, setIsWaiting] = useState(false);
  const [errorMsg, setErrorMsg] = useState('');
  const navigate = useNavigate();

  const getUserProfileById = async (userId) => {
    setErrorMsg('');
    setIsWaiting(true);
    const params = { userId: userId };
    try {
      const result = await agent.User.getUserProfile(params);
      setIsWaiting(false);
      return result.data;
    } catch (error) {
      setIsWaiting(false);
      setErrorMsg(error);
      return error;
    }
  };

  const getUnpublishedPosts = async () => {
    setErrorMsg('');
    setIsWaiting(true);
    try {
      const result = await agent.Post.getUnpublishedPosts();
      setIsWaiting(false);
      return result.data;
    } catch (error) {
      setIsWaiting(false);
      setErrorMsg(error);
      return error;
    }
  };

  const getThreePostWithMostReviews = async () => {
    setErrorMsg('');
    setIsWaiting(true);
    try {
      const result = await agent.Post.getThreePostWithMostReviews();
      setIsWaiting(false);
      return result.data;
    } catch (error) {
      setIsWaiting(false);
      setErrorMsg(error);
      return error;
    }
  };

  const getViewedPost = async () => {
    setErrorMsg('');
    setIsWaiting(true);
    try {
      const result = await agent.Composite.getViewedPost();
      setIsWaiting(false);
      return result.data;
    } catch (error) {
      setIsWaiting(false);
      setErrorMsg(error);
      return error;
    }
  };

  const getUserBannedPosts = async () => {
    setErrorMsg('');
    setIsWaiting(true);
    try {
      const result = await agent.Post.getUserBannedPosts();
      setIsWaiting(false);
      return result.data;
    } catch (error) {
      setIsWaiting(false);
      setErrorMsg(error);
      return error;
    }
  };

  const getUserPublishedPosts = async () => {
    setErrorMsg('');
    setIsWaiting(true);
    try {
      const result = await agent.Post.getUserPublishedPosts();
      setIsWaiting(false);
      return result.data;
    } catch (error) {
      setIsWaiting(false);
      setErrorMsg(error);
      return error;
    }
  };

  const getUserHiddenPosts = async () => {
    setErrorMsg('');
    setIsWaiting(true);
    try {
      const result = await agent.Post.getUserHiddenPosts();
      setIsWaiting(false);
      return result.data;
    } catch (error) {
      setIsWaiting(false);
      setErrorMsg(error);
      return error;
    }
  };

  const getUserDeletedPosts = async () => {
    setErrorMsg('');
    setIsWaiting(true);
    try {
      const result = await agent.Post.getUserDeletedPosts();
      setIsWaiting(false);
      return result.data;
    } catch (error) {
      setIsWaiting(false);
      setErrorMsg(error);
      return error;
    }
  };

  const updateUserProfile = async (userId, email, firstName, lastName, password, profileImage) => {
    setErrorMsg('');
    setIsWaiting(true);
    try {
      const formData = new FormData();
      formData.append('image', profileImage);
      const result = await agent.User.updateUserProfile(userId, firstName, lastName, email, password, formData);
      setIsWaiting(false);
      return result.data;
    } catch (error) {
      setIsWaiting(false);
      setErrorMsg(error);
      return error;
    }
  };

  const updateHistory = async (postId, isPublished) => {
    // update historyComposite
    setErrorMsg('');
    setIsWaiting(true);
    try {
      let result;
      if (isPublished) {
        result = await agent.Post.updateHistory({ postId: postId });
      }
      setIsWaiting(false);
      navigate(`/posts/${postId}`);
      return result.data;
    } catch (error) {
      setIsWaiting(false);
      setErrorMsg(error);
      return error;
    }
  };

  const getViewedOnDate = async (date) => {
    setErrorMsg('');
    setIsWaiting(true);
    try {
      const result = await agent.Composite.getViewedPostOnDate(date);
      setIsWaiting(false);
      return result.data;
    } catch (error) {
      setIsWaiting(false);
      setErrorMsg(error);
      return error;
    }
  };

  const getViewedContainWord = async (word) => {
    setErrorMsg('');
    setIsWaiting(true);
    try {
      const result = await agent.Composite.getViewedPostContainWord(word);
      setIsWaiting(false);
      return result.data;
    } catch (error) {
      setIsWaiting(false);
      setErrorMsg(error);
      return error;
    }
  };

  const sendVerificationEmail = async (email, id) => {
    setErrorMsg('');
    setIsWaiting(true);
    try {
      const body = { userId: id };
      const result = await agent.User.sendVerificationEmail(email, body);
      setIsWaiting(false);
      return result.data;
    } catch (error) {
      setIsWaiting(false);
      setErrorMsg(error);
      return error;
    }
  };

  return (
    <Layout
      isWaiting={isWaiting}
      errorMsg={errorMsg}
      getUserProfileById={getUserProfileById}
      getUnpublishedPosts={getUnpublishedPosts}
      getThreePostWithMostReviews={getThreePostWithMostReviews}
      getViewedPost={getViewedPost}
      updateUserProfile={updateUserProfile}
      getUserPublishedPosts={getUserPublishedPosts}
      getUserBannedPosts={getUserBannedPosts}
      getUserHiddenPosts={getUserHiddenPosts}
      getUserDeletedPosts={getUserDeletedPosts}
      updateHistory={updateHistory}
      getViewedOnDate={getViewedOnDate}
      getViewedContainWord={getViewedContainWord}
      sendVerificationEmail={sendVerificationEmail}
    />
  );
}

User.protoTypes = {};

export default User;
