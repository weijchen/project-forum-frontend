import React, { useState } from 'react';
import Layout from '../../components/User/UserEdit';
import agent from '../../lib/agent';
import PropTypes from 'prop-types';

function UserEdit(props) {
  const {
    show,
    onHide,
    firstName,
    lastName,
    email,
    password,
    setFirstName,
    setLastName,
    setEmail,
    setPassword,
    setProfileImage,
    handleUpdateUserProfile,
  } = props;
  const [isWaiting, setIsWaiting] = useState(false);
  const [errorMsg, setErrorMsg] = useState('');

  return (
    <Layout
      // isWaiting={isWaiting}
      // errorMsg={errorMsg}
      show={show}
      onHide={onHide}
      firstname={firstName}
      lastname={lastName}
      email={email}
      password={password}
      setfirstname={setFirstName}
      setlastname={setLastName}
      setemail={setEmail}
      setpassword={setPassword}
      setprofileimage={setProfileImage}
      handleupdateuserprofile={handleUpdateUserProfile}
    />
  );
}

UserEdit.propTypes = {
  show: PropTypes.bool.isRequired,
  onHide: PropTypes.func.isRequired,
  firstName: PropTypes.string.isRequired,
  lastName: PropTypes.string.isRequired,
  email: PropTypes.string.isRequired,
  password: PropTypes.string.isRequired,
  setFirstName: PropTypes.func.isRequired,
  setLastName: PropTypes.func.isRequired,
  setEmail: PropTypes.func.isRequired,
  setPassword: PropTypes.func.isRequired,
  setProfileImage: PropTypes.func.isRequired,
  handleUpdateUserProfile: PropTypes.func.isRequired,
};

export default UserEdit;
