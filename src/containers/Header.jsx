import React, {SetStateAction, Dispatch} from 'react';
import { BrowserRouter, Routes ,Route } from 'react-router-dom';


// class Header extends React.Component{
//     constructor(props) {
//         super(props);
//         this.state = {
//             tab:'/'
//         };
//     }



// render(){
// interface HeaderProps {
//     transtab: Dispatch<SetStateAction<boolean>>;
// }

function Header(props) {
    const onClickHomepageTabs = () =>{
        let tablists=document.getElementsByClassName("tabs-item");
        for (let i = 0; i < tablists.length; i++) {
            tablists[i].onclick=(e)=>{
                this.setState({tab: e.target.innerText});
            }
        }
        // eslint-disable-next-line react/prop-types
        props.transTab( this.state.tab);
        console.log("header:"+this.state.tab);
    }

        return(
            <div className="header-inner">
                {/*<div className="icon" onClick={this.handleClickBack}>*/}
                {/*    {!r && !rR &&<span></span>}*/}
                {/*    {r || rR &&<span className="iconfont iconleft-arrow"></span>}*/}
                {/*</div>*/}
                {/*This is header*/}
                <BrowserRouter>
                <Routes>
                    <Route path="/" element={
                        <div className="homePage-tabs tabBox">
                            <ul role="navigation" className="tabs" id="visitorPageUL">
                                <li role="tab" className="tabs-item" onClick={onClickHomepageTabs}>Login</li>
                                <li role="tab" className="tabs-item" onClick={onClickHomepageTabs}>Register</li>

                            </ul>
                        </div>
                    } />
                    <Route path="/user/*" element={
                        <div className="homePage-tabs tabBox">
                            <ul role="navigation" className="tabs" id="userPageUL">
                                <li role="tab" className="tabs-item" onClick={onClickHomepageTabs}>Home</li>
                                <li role="tab" className="tabs-item" onClick={onClickHomepageTabs}>Profile</li>
                                <li role="tab" className="tabs-item" onClick={onClickHomepageTabs}>Contact</li>
                                <li role="tab" className="tabs-item" onClick={onClickHomepageTabs}>Post</li>
                                <li role="tab" className="tabs-item" onClick={onClickHomepageTabs}>Logout</li>

                            </ul>
                        </div>
                    } />
                    <Route path="/admin/*" element={
                        <div className="homePage-tabs tabBox">
                            <ul role="navigation" className="tabs" id="userPageUL">
                                <li role="tab" className="tabs-item" onClick={onClickHomepageTabs}>Home</li>
                                <li role="tab" className="tabs-item" onClick={onClickHomepageTabs}>Message</li>
                                <li role="tab" className="tabs-item" onClick={onClickHomepageTabs}>User</li>
                                <li role="tab" className="tabs-item" onClick={onClickHomepageTabs}>Logout</li>
                            </ul>
                        </div>
                    } />

                </Routes>
                </BrowserRouter>

            </div>
        )
    // }



}
export default Header;
