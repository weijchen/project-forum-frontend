import React, { useState } from 'react';
import agent from '../../lib/agent';
import Layout from '../../components/Message/MessageManagement';

function MessageManagement(props) {
  const [isWaiting, setIsWaiting] = useState(false);
  const [errorMsg, setErrorMsg] = useState(null);

  const getAllMessages = async () => {
    setErrorMsg(null);
    setIsWaiting(true);
    try {
      const result = await agent.Message.getAllMessages();
      setIsWaiting(false);
      return result.data;
    } catch (error) {
      setIsWaiting(false);
      setErrorMsg(error);
      return error;
    }
  };

  const updateMessageStatus = async (id) => {
    setErrorMsg(null);
    setIsWaiting(true);
    try {
      const result = await agent.Message.updateMessageStatus(id);
      setIsWaiting(false);
      return result.data;
    } catch (error) {
      setIsWaiting(false);
      setErrorMsg(error);
      return error;
    }
  };

  return (
    <Layout
      isWaiting={isWaiting}
      errorMsg={errorMsg}
      getAllMessages={getAllMessages}
      updateMessageStatus={updateMessageStatus}
    />
  );
}

MessageManagement.protoTypes = {};

export default MessageManagement;
