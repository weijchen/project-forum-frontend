import React, { useState } from 'react';
import agent from '../../lib/agent';
import Layout from '../../components/Message/ContactUs';

function ContactUs(props) {
  const [isWaiting, setIsWaiting] = useState(false);
  const [errorMsg, setErrorMsg] = useState(null);

  const createMessage = async (subject, email, message) => {
    setErrorMsg('null');
    setIsWaiting(true);
    try {
      const body = { subject: subject, email: email, message: message };
      const result = await agent.Message.createMessage(body);
      setIsWaiting(false);
      return result.data;
    } catch (error) {
      setIsWaiting(false);
      setErrorMsg(error);
      return error;
    }
  };

  return <Layout isWaiting={isWaiting} errorMsg={errorMsg} createMessage={createMessage} />;
}

ContactUs.protoTypes = {};

export default ContactUs;
