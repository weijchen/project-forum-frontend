import React, {useState} from 'react';
import UserHome from '../../components/Home/UserHome.jsx';
import agent from "../../lib/agent.js";
import {useSelector} from "react-redux";
import AdminHome from "../../components/Home/AdminHome.jsx";

function Home(props) {
  const [isWaiting, setIsWaiting] = useState(false);
  const [errorMsg, setErrorMsg] = useState('');
  const userInfo = useSelector((state) => state.auth.userInfo);

  return (
      <div>
        { (userInfo.role ==='NORMAL_USER' || userInfo.role ==='NORMAL_USER_NOT_VALID') ? (
            <UserHome isWaiting={isWaiting} errorMsg={errorMsg} />
        ) : (
            <AdminHome />
        )}
      </div>
  );

  
}


Home.protoTypes = {};

export default Home;
