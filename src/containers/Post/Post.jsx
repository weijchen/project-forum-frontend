import React, { useState } from 'react';
import Layout from '../../components/Post/Post';
import agent from '../../lib/agent';

function Post(props) {
  const [isWaiting, setIsWaiting] = useState(false);
  const [errorMsg, setErrorMsg] = useState(null);

  return <Layout isWaiting={isWaiting} errorMsg={errorMsg} />;
}

Post.protoTypes = {};

export default Post;
