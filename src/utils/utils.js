export const dateParser = (dateString) => {
  return new Date(dateString).toDateString();
};

export const resolveToken = () => {
  const token = localStorage.getItem('token');
  if (token) {
    // Split the token to get the payload
    const [, payloadBase64] = token.split('.');
    // Decode the base64 payload
    console.log(payloadBase64);
    const decodedPayload = atob(payloadBase64);

    // Parse the decoded payload to get user information
    const user = JSON.parse(decodedPayload);

    return user;
  }

  return null;
};

export const PostStatus = {
  Unpublished: 0,
  Published: 1,
  Hidden: 2,
  Banned: 3,
  Deleted: 4,
};

export const PostStatusStr = {
  Unpublished: 'Unpublished',
  Published: 'Published',
  Hidden: 'Hidden',
  Banned: 'Banned',
  Deleted: 'Deleted',
};

export const toUserType = (number) => {
  return Object.keys(PostStatusStr)[number];
};
